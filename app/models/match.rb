class Match < ApplicationRecord
  belongs_to :fixture, inverse_of: :matches
  
  has_many :innings
end
