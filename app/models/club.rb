class Club < ApplicationRecord
    validates :name, :street_address, :street_city, :street_state, :phone, :email, presence: true
    mount_uploader :logo, LogoUploader

    has_many :teams
    has_many :players

    def display_address
      if self.street_address != nil then
        return self.street_address.to_s + ", " + self.street_city.to_s + ", " + self.street_state.to_s + ", " + self.street_postcode.to_s
      end
    end
    def display_post_address
      if self.postal_address != nil then
        return self.postal_address + ", " + self.postal_city + ", " + self.postal_state + ", " + self.postal_postcode
      end
    end
end
