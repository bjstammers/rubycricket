class Keyword < ApplicationRecord
  validates :key_type, :key_value, presence: true

  def by_key(key)
      AdminKeyword.find_each do |keyword|
          keyword.find_by key_type: (key)
      end
  end
end
