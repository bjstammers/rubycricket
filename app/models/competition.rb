class Competition < ApplicationRecord
    validates :name, :start_date, :end_date, :match_days, presence: true

    has_many :rounds, inverse_of: :competition
    # has_many :matches
end
