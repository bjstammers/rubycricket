class Inning < ApplicationRecord
  validates :inning_no, :batting_team, :bowling_team, presence: true  
  
  belongs_to :match, inverse_of: :innings
  #has_many :overs
end
