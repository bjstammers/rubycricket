class Player < ApplicationRecord
  validates :first_name, :last_name, :full_name, :display_name, :phone, :email, presence: :true

  belongs_to :club
  belongs_to :team

  # def club_name
  #   return Club.find_by_id(self.club_id).name
  # end
end
