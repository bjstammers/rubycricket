class Team < ApplicationRecord
  validates :name, :grade, presence: true

  belongs_to :club
  has_many :players
  
  # def club_name
  #   return Club.find(self.club_id).name
  # end
end
