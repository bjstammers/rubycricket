class Round < ApplicationRecord
  validates :round_no, :start_date, :end_date, presence: true
  
  belongs_to :competition, inverse_of: :rounds
  has_many :fixtures, dependent: :destroy, inverse_of: :round
  accepts_nested_attributes_for :fixtures
  
  def all_matches
    self.fixtures.each do |fix|
      return fix.matches
    end
  end
end
