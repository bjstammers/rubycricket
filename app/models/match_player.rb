class MatchPlayer < ApplicationRecord
    validates :name, presence: true
    
    belongs_to :match
end
