class Fixture < ApplicationRecord
    validates :grade, :home_team, :away_team, presence: true
    
    belongs_to :round, inverse_of: :fixtures
    has_many :matches, inverse_of: :fixture
end
