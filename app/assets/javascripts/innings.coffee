# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'turbolinks:load', ->
    $('#graph-expand-row').hide()
    $('#details-expand-row').hide()
    $('#batting-expand-row').hide()
    $('#fow-expand-row').hide()
    $('#bowling-expand-row').hide()
    $('#graph_collapse').click ->
        $('#graph_details_row').hide()
        #$('#graph_header').addClass('inverted')
        $('#graph-collapse-row').hide()
        $('#graph-expand-row').show()
    $('#graph_expand').click ->
        $('#graph_details_row').show()
        #$('#graph_header').removeClass('inverted')
        $('#graph-collapse-row').show()
        $('#graph-expand-row').hide()
    $('#details_collapse').click ->
        $('#inning_details_row1').hide()
        $('#inning_details_row2').hide()
        $('#inning_details_row3').hide()
        #$('#details_header').addClass('inverted')
        $('#details-collapse-row').hide()
        $('#details-expand-row').show()
    $('#details_expand').click ->
        $('#inning_details_row1').show()
        $('#inning_details_row2').show()
        $('#inning_details_row3').show()
        #$('#details_header').removeClass('inverted')
        $('#details-collapse-row').show()
        $('#details-expand-row').hide()
    $('#batting_collapse').click ->
        $('#batting_details_row').hide()
        $('#batting_details_table').hide()
        #$('#batting_header').addClass('inverted')
        $('#batting-collapse-row').hide()
        $('#batting-expand-row').show()
    $('#batting_expand').click ->
        $('#batting_details_row').show()
        $('#batting_details_table').show()
        #$('#batting_header').removeClass('inverted')
        $('#batting-collapse-row').show()
        $('#batting-expand-row').hide()
    $('#fow_collapse').click ->
        $('#fow_details_row').hide()
        $('#fow_details_table').hide()
        #$('#fow_header').addClass('inverted')
        $('#fow-collapse-row').hide()
        $('#fow-expand-row').show()
    $('#fow_expand').click ->
        $('#fow_details_row').show()
        $('#fow_details_table').show()
        #$('#fow_header').removeClass('inverted')
        $('#fow-collapse-row').show()
        $('#fow-expand-row').hide()
    $('#bowling_collapse').click ->
        $('#bowling_details_row').hide()
        $('#bowling_details_table').hide()
        #$('#bowling_header').addClass('inverted')
        $('#bowling-collapse-row').hide()
        $('#bowling-expand-row').show()
    $('#bowling_expand').click ->
        $('#bowling_details_row').show()
        $('#bowling_details_table').show()
        #$('#bowling_header').removeClass('inverted')
        $('#bowling-collapse-row').show()
        $('#bowling-expand-row').hide()
        
jQuery ->
    data = {
        labels : ["01", "02", "03", "04"],
        datasets : [
            {
                fillColor: "rgba(220,220,220,0.5)",
                strokeColor : "rgba(220,220,220,1)",
                pointColor : "rgba(220,220,220,1)",
                pointStrokeColor : "#fff",
                data : [1,4,6,2],
            }
        ]
    }
    options = {}
    

