# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'turbolinks:load', ->
    $('#competition_start_date').datepicker({
        dateFormat: 'dd/mm/yy',
    })
    $('#competition_end_date').datepicker({
        dateFormat: 'dd/mm/yy',
    })
    if $('#competition_limited_overs').attr('value') != 1
        $('#limited_over_details').hide()
    if $('#competition_allow_bonus').attr('value') != 1
        $('#bonus_points_details').hide()
    if $('#limited_overs').html() == "No"
        $('#limited_over_details').hide()
    else
        $('#limited_over_details').show()
    if $('#allow_bonus').html() == "No"
        $('#bonus_points_details').hide()
    else
        $('#bonus_points_details').show()
    
    $('#competition_limited_overs').click ->
        if $(this).is(':checked')
            $('#limited_over_details').show()
        else
            $('#limited_over_details').hide()
        return
    $('#competition_allow_bonus').click ->
        if $(this).is(':checked')
            $('#bonus_points_details').show()
        else
            $('#bonus_points_details').hide()
        return
        
    return
    

# $(document).ready ->
#     alert("val = " + $('#competition_limited_overs').value())
#     alert("txt = " + $('#competition_limited_overs').text())
#     $('#competition_limited_overs').click ->
#         if $(this).is(':checked')
#             alert("checked")
#             $('#limited_over_details').show()
#             # alert("val = " + $('#competition_limited_overs').value()
#             # alert("txt = " + $('#competition_limited_overs').value()
#         else
#             alert("unchecked")
#             $('#limited_over_details').hide()
#         return
#     return

