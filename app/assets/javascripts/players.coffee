# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'turbolinks:load', ->
   $('#player_birth_date').datepicker({
       dateFormat: 'dd/mm/yy',
    })
   $('#player_date_joined').datepicker({
       dateFormat: 'dd/mm/yy',
    })
   return

