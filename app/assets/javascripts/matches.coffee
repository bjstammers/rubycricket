# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'turbolinks:load', ->
    #initially hide edit rows
    i = 1
    while i <= 13
        itm_h = "player_home_id_" + i.toString()
        sel_h = "home_select_" + i.toString()
        edt_h = "home_edit_" + i.toString()
        itm_a = "player_away_id_" + i.toString()
        sel_a = "away_select_" + i.toString()
        edt_a = "away_edit_" + i.toString()
        if $('#' + itm_h).val() == "" || $('#' + itm_h).val() == undefined
            if $('#player_home_' + i.toString()).val() == "" || $('#player_home_' + i.toString()).val() == ""
                $('#' + edt_h).hide()
            else
                $('#' + sel_h).hide()
        else
            $('#' + edt_h).hide()
        if $('#' + itm_a).val() == "" || $('#' + itm_a).val() == undefined
            if $('#player_away_' + i.toString()).val() == "" || $('#player_away_' + i.toString()).val() == undefined
                $('#' + edt_a).hide()
            else
                $('#' + sel_a).hide()
        else
            $('#' + edt_a).hide()
        i++
    #process clicks
    $('#hide_home_select_1').click ->
        $('#home_edit_1').show()
        $('#home_select_1').hide()
        $('#player_home_id_1').val("")
        return
    $('#hide_home_edit_1').click ->
        $('#home_edit_1').hide()
        $('#home_select_1').show()
        $('#player_home_1').val("")
        return
    $('#hide_home_select_2').click ->
        $('#home_edit_2').show()
        $('#home_select_2').hide()
        $('#player_home_id_2').val("")
        return
    $('#hide_home_edit_2').click ->
        $('#home_edit_2').hide()
        $('#home_select_2').show()
        $('#player_home_2').val("")
        return
    $('#hide_home_select_3').click ->
        $('#home_edit_3').show()
        $('#home_select_3').hide()
        $('#player_home_id_3').val("")
        return
    $('#hide_home_edit_3').click ->
        $('#home_edit_3').hide()
        $('#home_select_3').show()
        $('#player_home_3').val("")
        return
    $('#hide_home_select_4').click ->
        $('#home_edit_4').show()
        $('#home_select_4').hide()
        $('#player_home_id_4').val("")
        return
    $('#hide_home_edit_4').click ->
        $('home_#edit_4').hide()
        $('#home_select_4').show()
        $('#player_home_4').val("")
        return
    $('#hide_home_select_5').click ->
        $('#home_edit_5').show()
        $('#home_select_5').hide()
        $('#player_home_id_5').val("")
        return
    $('#hide_home_edit_5').click ->
        $('#home_edit_5').hide()
        $('#home_select_5').show()
        $('#player_home_5').val("")
        return
    $('#hide_home_select_6').click ->
        $('#home_edit_6').show()
        $('#home_select_6').hide()
        $('#player_home_id_6').val("")
        return
    $('#hide_home_edit_6').click ->
        $('#home_edit_6').hide()
        $('#home_select_6').show()
        $('#player_home_6').val("")
        return
    $('#hide_home_select_7').click ->
        $('#home_edit_7').show()
        $('#home_select_7').hide()
        $('#player_home_id_7').val("")
        return
    $('#hide_home_edit_7').click ->
        $('#home_edit_7').hide()
        $('#home_select_7').show()
        $('#player_home_7').val("")
        return
    $('#hide_home_select_8').click ->
        $('#home_edit_8').show()
        $('#home_select_8').hide()
        $('#player_home_id_8').val("")
        return
    $('#hide_home_edit_8').click ->
        $('#home_edit_8').hide()
        $('#home_select_8').show()
        $('#player_home_8').val("")
        return
    $('#hide_home_select_9').click ->
        $('#home_edit_9').show()
        $('#home_select_9').hide()
        $('#player_home_id_9').val("")
        return
    $('#hide_home_edit_9').click ->
        $('#home_edit_9').hide()
        $('#home_select_9').show()
        $('#player_home_9').val("")
        return
    $('#hide_home_select_10').click ->
        $('#home_edit_10').show()
        $('#home_select_10').hide()
        $('#player_home_id_10').val("")
        return
    $('#hide_home_edit_10').click ->
        $('#home_edit_10').hide()
        $('#home_select_10').show()
        $('#player_home_10').val("")
        return
    $('#hide_home_select_11').click ->
        $('#home_edit_11').show()
        $('#home_select_11').hide()
        $('#player_home_id_11').val("")
        return
    $('#hide_home_edit_11').click ->
        $('#home_edit_11').hide()
        $('#home_select_11').show()
        $('#player_home_11').val("")
        return
    $('#hide_home_select_12').click ->
        $('#home_edit_12').show()
        $('#home_select_12').hide()
        $('#player_home_id_12').val("")
        return
    $('#hide_home_edit_12').click ->
        $('#home_edit_12').hide()
        $('#home_select_12').show()
        $('#player_home_12').val("")
        return
    $('#hide_home_select_13').click ->
        $('#home_edit_13').show()
        $('#home_select_13').hide()
        $('#player_home_id_13').val("")
        return
    $('#hide_home_edit_13').click ->
        $('#home_edit_13').hide()
        $('#home_select_13').show()
        $('#player_home_13').val("")
        return
     $('#hide_away_select_1').click ->
        $('#away_edit_1').show()
        $('#away_select_1').hide()
        $('#player_away_id_1').val("")
        return
    $('#hide_away_edit_1').click ->
        $('#away_edit_1').hide()
        $('#away_select_1').show()
        $('#player_away_1').val("")
        return
    $('#hide_away_select_2').click ->
        $('#away_edit_2').show()
        $('#away_select_2').hide()
        $('#player_away_id_2').val("")
        return
    $('#hide_away_edit_2').click ->
        $('#away_edit_2').hide()
        $('#away_select_2').show()
        $('#player_away_2').val("")
        return
    $('#hide_away_select_3').click ->
        $('#away_edit_3').show()
        $('#away_select_3').hide()
        $('#player_away_id_3').val("")
        return
    $('#hide_away_edit_3').click ->
        $('#away_edit_3').hide()
        $('#away_select_3').show()
        $('#player_away_3').val("")
       return
    $('#hide_away_select_4').click ->
        $('#away_edit_4').show()
        $('#away_select_4').hide()
        $('#player_away_id_4').val("")
        return
    $('#hide_away_edit_4').click ->
        $('away_#edit_4').hide()
        $('#away_select_4').show()
        $('#player_away_4').val("")
       return
    $('#hide_away_select_5').click ->
        $('#away_edit_5').show()
        $('#away_select_5').hide()
        $('#player_away_id_5').val("")
        return
    $('#hide_away_edit_5').click ->
        $('#away_edit_5').hide()
        $('#away_select_5').show()
        $('#player_away_5').val("")
        return
    $('#hide_away_select_6').click ->
        $('#away_edit_6').show()
        $('#away_select_6').hide()
        $('#player_away_id_6').val("")
        return
    $('#hide_away_edit_6').click ->
        $('#away_edit_6').hide()
        $('#away_select_6').show()
        $('#player_away_6').val("")
        return
    $('#hide_away_select_7').click ->
        $('#away_edit_7').show()
        $('#away_select_7').hide()
        $('#player_away_id_7').val("")
        return
    $('#hide_away_edit_7').click ->
        $('#away_edit_7').hide()
        $('#away_select_7').show()
        $('#player_away_7').val("")
        return
    $('#hide_away_select_8').click ->
        $('#away_edit_8').show()
        $('#away_select_8').hide()
        $('#player_away_id_8').val("")
        return
    $('#hide_away_edit_8').click ->
        $('#away_edit_8').hide()
        $('#away_select_8').show()
        $('#player_away_8').val("")
        return
    $('#hide_away_select_9').click ->
        $('#away_edit_9').show()
        $('#away_select_9').hide()
        $('#player_away_id_9').val("")
        return
    $('#hide_away_edit_9').click ->
        $('#away_edit_9').hide()
        $('#away_select_9').show()
        $('#player_away_9').val("")
        return
    $('#hide_away_select_10').click ->
        $('#away_edit_10').show()
        $('#away_select_10').hide()
        $('#player_away_id_10').val("")
        return
    $('#hide_away_edit_10').click ->
        $('#away_edit_10').hide()
        $('#away_select_10').show()
        $('#player_away_10').val("")
        return
    $('#hide_away_select_11').click ->
        $('#away_edit_11').show()
        $('#away_select_11').hide()
        $('#player_away_id_11').val("")
        return
    $('#hide_away_edit_11').click ->
        $('#away_edit_11').hide()
        $('#away_select_11').show()
        $('#player_away_11').val("")
        return
    $('#hide_away_select_12').click ->
        $('#away_edit_12').show()
        $('#away_select_12').hide()
        $('#player_away_id_12').val("")
        return
    $('#hide_away_edit_12').click ->
        $('#away_edit_12').hide()
        $('#away_select_12').show()
        $('#player_away_12').val("")
        return
    $('#hide_away_select_13').click ->
        $('#away_edit_13').show()
        $('#away_select_13').hide()
        $('#player_away_id_13').val("")
        return
    $('#hide_away_edit_13').click ->
        $('#away_edit_13').hide()
        $('#away_select_13').show()
        $('#player_away_13').val("")
        return
    
