json.extract! inning, :id, :inning_no, :batting_team, :bowling_team, :runs, :wickets, :byes, :legbyes, :no_balls, :wides, :score, :match_id, :created_at, :updated_at
json.url inning_url(inning, format: :json)
