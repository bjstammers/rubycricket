json.extract! round, :id, :round_no, :start_date, :end_date, :competition_id, :created_at, :updated_at
json.url round_url(round, format: :json)
