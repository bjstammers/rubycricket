json.extract! team, :id, :name, :grade, :captain, :vice_captain, :club_id, :created_at, :updated_at
json.url team_url(team, format: :json)
