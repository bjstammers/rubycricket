json.extract! match, :id, :fixture_id, :referee1, :referee2, :scorer, :result, :created_at, :updated_at
json.url match_url(match, format: :json)
