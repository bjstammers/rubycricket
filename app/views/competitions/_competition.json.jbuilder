json.extract! competition, :id, :name, :start_date, :end_date, :match_days, :limited_overs, :restrict_bowl, :restrict_bat, :restrict_field, :points_win, :points_draw, :points_lose, :allow_bonus, :bonus_criteria, :points_bonus, :created_at, :updated_at
json.url competition_url(competition, format: :json)
