json.extract! keyword, :id, :key_type, :key_subtype, :key_value, :created_at, :updated_at
json.url keyword_url(keyword, format: :json)
