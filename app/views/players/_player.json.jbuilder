json.extract! player, :id, :first_name, :last_name, :full_name, :display_name, :phone, :email, :birth_date, :club_member, :date_joined, :club_id, :team_id, :created_at, :updated_at
json.url player_url(player, format: :json)
