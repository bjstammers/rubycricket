class MatchPlayersController < ApplicationController
  before_action :set_match_player, only: [:show, :edit, :update, :destroy]
    
    def index
        @match_players = MatchPlayer.all
    end

  def show
  end 
  
  def list
    i = 0
    @players = MatchPlayer.where("match_id = ? AND team = ?", params[:match_id],params[:team])
   
    @players.each do |mp|
      i += 1
      sel = "player_select_" + i.to_s
      edt = "player_edit_" + i.to_s
      if mp.player_id = ""
        puts edt + " = " + mp.name
#        params[edt] = mp.name
        match_player_params[edt] = mp.name
        puts match_player_params[edt]
      else
        puts sel + " = " + mp.name
        match_player_params[sel] = mp.name
        puts match_player_params[sel]
      end
    end
  end

  def new
      session[:team] = params[:team]
      session[:match_id] = params[:match_id]
      @match_player = MatchPlayer.new
  end

  def edit
  end

  def create
      puts "start create"
      for i in 1..13
        sel = "player_select_" + i.to_s
        edt = "player_edit_" + i.to_s
        @match_player = MatchPlayer.new
        @match_player.team = session[:team]
        @match_player.match_id = session[:match_id]
        @match_player.position = i
        if match_player_params.fetch(sel).to_s != ""
          @match_player.name = Player.find(match_player_params.fetch(sel)).display_name
          @match_player.player_id = match_player_params.fetch(sel)
        end
        if match_player_params.fetch(edt).to_s != ""
          @match_player.name = match_player_params.fetch(edt).to_s
        end
        if @match_player.save
          check = true
        else
          if i == 1
            check = false
          else
            check = true
          end
          break
        end
      end 
      respond_to do |format|
        if check
          puts "save successful"
          format.html { redirect_to fixture_matches_path(:fixture_id), notice: 'Match players were successfully created.' }
          format.json { render :show, status: :created, location: @match }
        else
          puts "save not successful"
          format.html { render :new }
          format.json { render json: @match.errors, status: :unprocessable_entity }
        end
      end
  end

  def update
  end

  def destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_match_player
        @match_player = MatchPlayer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def match_player_params
        params.require(:match_player).permit(:match_id, :name, :team, :player_id, :player_select_1, :player_edit_1, :player_select_2, :player_edit_2, :player_select_3, :player_edit_3, :player_select_4, :player_edit_4, :player_select_5, :player_edit_5, :player_select_6, :player_edit_6, :player_select_7, :player_edit_7, :player_select_8, :player_edit_8, :player_select_9, :player_edit_9, :player_select_10, :player_edit_10, :player_select_11, :player_edit_11, :player_select_12, :player_edit_12, :player_select_13, :player_edit_13)
    end

    def list_match_players
      
    end
end
