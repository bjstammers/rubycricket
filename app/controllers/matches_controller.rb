class MatchesController < ApplicationController
  before_action :set_match, only: [:show, :edit, :update, :destroy]

  # GET /matches
  # GET /matches.json
  def index
    @matches = Match.all
  end

  # GET /matches/1
  # GET /matches/1.json
  def show
  end

  # GET /matches/new
  def new
    session[:fixture] = params[:fixture_id]
    @match = Match.new
    @match.fixture_id = session[:fixture]
    @fixture = Fixture.find(session[:fixture])
    @round = @fixture.round
  end

  # GET /matches/1/edit
  def edit
  end

  # POST /matches
  # POST /matches.json
  def create
#     for i in 1..13
#       sel_h = "player_home_select_" + i.to_s
#       edt_h = "player_home_edit_" + i.to_s
#       sel_a = "player_away_select_" + i.to_s
#       edt_a = "player_away_edit_" + i.to_s
#       if match_params.fetch(sel_h).to_s != ""
# #        @match.send("player_home_" + i.to_s, Player.find(match_params.fetch(sel_h)).display_name)
# #        @match.send("player_home_id_" + i.to_s, match_params.fetch(sel_h))
#         match_params.send("player_home_" + i.to_s, Player.find(match_params.fetch(sel_h)).display_name)
#         match_params.send("player_home_id_" + i.to_s, match_params.fetch(sel_h))
#       end
#       if match_params.fetch(edt_h).to_s != ""
# #        @match.send("player_home_" + i.to_s, match_params.fetch(edt_h).to_s)
#         match_params.send("player_home_" + i.to_s, match_params.fetch(edt_h).to_s)
#       end
#       if match_params.fetch(sel_a).to_s != ""
# #        @match.send("player_away_" + i.to_s, Player.find(match_params.fetch(sel_a)).display_name)
# #        @match.send("player_away_id_" + i.to_s, match_params.fetch(sel_a))
#         match_params.send("player_away_" + i.to_s, Player.find(match_params.fetch(sel_a)).display_name)
#         match_params.send("player_away_id_" + i.to_s, match_params.fetch(sel_a))
#       end
#       if match_params.fetch(edt_a).to_s != ""
# #        @match.send("player_away_" + i.to_s, match_params.fetch(edt_a).to_s)
#         match_params.send("player_away_" + i.to_s, match_params.fetch(edt_a).to_s)
#       end
#     end 
    @match = Match.new(match_params)
    @match.fixture_id = session[:fixture]
    #add names for selected players
    for i in 1..13
      fld_h = "player_home_id_" + i.to_s
      fld_a = "player_away_id_" + i.to_s
      puts fld_h
      puts fld_a
      if !@match[fld_h].blank?
        puts "@match[" + fld_h + "] = " + @match[fld_h].to_s
        @match["player_home_" + i.to_s] = Player.find(@match[fld_h]).display_name
        puts "player name = " + @match["player_home_" + i.to_s]
      end
      if !@match[fld_a].blank?
        puts "@match[" + fld_a + "] = " + @match[fld_a].to_s
        @match["player_away_" + i.to_s] = Player.find(@match[fld_a]).display_name
        puts "player name = " + @match["player_away_" + i.to_s]
      end
    end

    respond_to do |format|
      puts "pre save"
      if @match.save
        puts "save successful"
        format.html { redirect_to fixture_matches_path(:fixture_id), notice: 'Match was successfully created.' }
        format.json { render :show, status: :created, location: @match }
      else
        puts "save not successful"
        format.html { render :new }
        format.json { render json: @match.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /matches/1
  # PATCH/PUT /matches/1.json
  def update
    respond_to do |format|
      if @match.update(match_params)
        format.html { redirect_to @match, notice: 'Match was successfully updated.' }
        format.json { render :show, status: :ok, location: @match }
      else
        format.html { render :edit }
        format.json { render json: @match.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /matches/1
  # DELETE /matches/1.json
  def destroy
    @match.destroy
    respond_to do |format|
      format.html { redirect_to fixture_matches_path(:fixture_id), notice: 'Match was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_match
      @match = Match.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def match_params
      params.require(:match).permit(:fixture_id, :referee1, :referee2, :scorer, :result,
       :player_home_1, :player_home_id_1, :player_home_2, :player_home_id_2, :player_home_3, :player_home_id_3, :player_home_4, :player_home_id_4, :player_home_5, :player_home_id_5, :player_home_6, :player_home_id_6, :player_home_7, :player_home_id_7, :player_home_8, :player_home_id_8, :player_home_9, :player_home_id_9, :player_home_10, :player_home_id_10, :player_home_11, :player_home_id_11, :player_home_12, :player_home_id_12, :player_home_13, :player_home_id_13,
       :player_away_1, :player_away_id_1, :player_away_2, :player_away_id_2, :player_away_3, :player_away_id_3, :player_away_4, :player_away_id_4, :player_away_5, :player_away_id_5, :player_away_6, :player_away_id_6, :player_away_7, :player_away_id_7, :player_away_8, :player_away_id_8, :player_away_9, :player_away_id_9, :player_away_10, :player_away_id_10, :player_away_11, :player_away_id_11, :player_away_12, :player_away_id_12, :player_away_13, :player_away_id_13,
      )
    end
    # def match_params
    #   params.require(:match).permit(:fixture_id, :referee1, :referee2, :scorer, :result,
    #   :player_home_1, :player_home_id_1, :player_home_2, :player_home_id_2, :player_home_3, :player_home_id_3, :player_home_4, :player_home_id_4, :player_home_5, :player_home_id_5, :player_home_6, :player_home_id_6, :player_home_7, :player_home_id_7, :player_home_8, :player_home_id_8, :player_home_9, :player_home_id_9, :player_home_10, :player_home_id_10, :player_home_11, :player_home_id_11, :player_home_12, :player_home_id_12, :player_home_13, :player_home_id_13,
    #   :player_away_1, :player_away_id_1, :player_away_2, :player_away_id_2, :player_away_3, :player_away_id_3, :player_away_4, :player_away_id_4, :player_away_5, :player_away_id_5, :player_away_6, :player_away_id_6, :player_away_7, :player_away_id_7, :player_away_8, :player_away_id_8, :player_away_9, :player_away_id_9, :player_away_10, :player_away_id_10, :player_away_11, :player_away_id_11, :player_away_12, :player_away_id_12, :player_away_13, :player_away_id_13,
    #   :player_home_select_1, :player_home_edit_1, :player_home_select_2, :player_home_edit_2, :player_home_select_3, :player_home_edit_3, :player_home_select_4, :player_home_edit_4, :player_home_select_5, :player_home_edit_5, :player_home_select_6, :player_home_edit_6, :player_home_select_7, :player_home_edit_7, :player_home_select_8, :player_home_edit_8, :player_home_select_9, :player_home_edit_9, :player_home_select_10, :player_home_edit_10, :player_home_select_11, :player_home_edit_11, :player_home_select_12, :player_home_edit_12, :player_home_select_13, :player_home_edit_13,
    #   :player_away_select_1, :player_away_edit_1, :player_away_select_2, :player_away_edit_2, :player_away_select_3, :player_away_edit_3, :player_away_select_4, :player_away_edit_4, :player_away_select_5, :player_away_edit_5, :player_away_select_6, :player_away_edit_6, :player_away_select_7, :player_away_edit_7, :player_away_select_8, :player_away_edit_8, :player_away_select_9, :player_away_edit_9, :player_away_select_10, :player_away_edit_10, :player_away_select_11, :player_away_edit_11, :player_away_select_12, :player_away_edit_12, :player_away_select_13, :player_away_edit_13,
    #   )
    #end
end
