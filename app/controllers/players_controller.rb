class PlayersController < ApplicationController
  before_action :set_player, only: [:show, :edit, :update, :destroy]

  # GET /players
  # GET /players.json
  def index
    if params[:club_id].to_s == "" 
      if params[:team_id].to_s == ""
        @players = Player.all
      else
        @players = Player.where(team_id: params[:team_id])
      end
    else
      @players = Player.where(club_id: params[:club_id])
    end
  end

  # GET /players/1
  # GET /players/1.json
  def show
  end

  # GET /players/new
  def new
    if params[:club_id].to_s == "" 
      if params[:team_id].to_s != ""
        team_id = params[:team_id]
        club_id = Team.find(team_id).club_id
      end
    else
      club_id = params[:club_id]
    end
    @clubs = Club.all
    @teams = Team.all
    @player = Player.new
    @player.team_id = team_id
    @player.club_id = club_id
  end

  # GET /players/1/edit
  def edit
    @clubs = Club.all
    @teams = Team.all
  end

  # POST /players
  # POST /players.json
  def create
    @clubs = Club.all
    @player = Player.new(player_params)

    respond_to do |format|
      if @player.save
        format.html { redirect_to @player, notice: 'Player was successfully created.' }
        format.json { render :show, status: :created, location: @player }
      else
        format.html { render :new }
        format.json { render json: @player.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /players/1
  # PATCH/PUT /players/1.json
  def update
    @clubs = Club.all
    respond_to do |format|
      if @player.update(player_params)
        format.html { redirect_to @player, notice: 'Player was successfully updated.' }
        format.json { render :show, status: :ok, location: @player }
      else
        format.html { render :edit }
        format.json { render json: @player.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /players/1
  # DELETE /players/1.json
  def destroy
    @player.destroy
    respond_to do |format|
      format.html { redirect_to players_url, notice: 'Player was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_player
      @player = Player.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def player_params
      params.require(:player).permit(:first_name, :last_name, :full_name, :display_name, :phone, :email, :birth_date, :club_member, :date_joined, :club_id, :team_id)
    end
end
