Rails.application.routes.draw do
  
  resources :innings
  #get 'match_players/index'

  #get 'match_players/show'

  get 'fixtures/index'

  get 'fixtures/show'
  
  get 'matches/all', to: 'matches#all'

  resources :matches
  resources :rounds
  # resources :comp_rounds
  resources :competitions, shallow: true do
    resources :rounds do
      resources :fixtures do
        resources :matches do
          resources :innings do
          
          end
        end  
      end
    end
  end
  resources :players
  resources :teams

  resources :clubs, shallow: true do
    resources :teams do
      resources :players
    end
    resources :players
  end
  resources :keywords
  #get 'static_pages/welcome'
  root 'static_pages#welcome'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
