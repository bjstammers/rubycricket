# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181011004052) do

  create_table "clubs", force: :cascade do |t|
    t.string "name"
    t.string "street_address"
    t.string "street_city"
    t.string "street_state"
    t.string "street_postcode"
    t.string "postal_address"
    t.string "postal_city"
    t.string "postal_state"
    t.string "postal_postcode"
    t.string "phone"
    t.string "fax"
    t.string "email"
    t.string "website"
    t.string "president_name"
    t.string "president_contact"
    t.string "chairman_name"
    t.string "chairman_contact"
    t.string "secretary_name"
    t.string "secretary_contact"
    t.string "treasurer_name"
    t.string "treasurer_contact"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "logo"
  end

  create_table "comp_rounds", force: :cascade do |t|
    t.string "round_no"
    t.date "start_date"
    t.date "end_date"
    t.integer "competition_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["competition_id"], name: "index_comp_rounds_on_competition_id"
  end

  create_table "competitions", force: :cascade do |t|
    t.string "name"
    t.date "start_date"
    t.date "end_date"
    t.integer "match_days"
    t.boolean "limited_overs"
    t.string "restrict_bowl"
    t.string "restrict_bat"
    t.string "restrict_field"
    t.float "points_win"
    t.float "points_draw"
    t.float "points_lose"
    t.boolean "allow_bonus"
    t.string "bonus_criteria"
    t.float "points_bonus"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "max_overs"
    t.integer "max_bowler_overs"
  end

  create_table "fixtures", force: :cascade do |t|
    t.string "home_team"
    t.string "away_team"
    t.string "ground"
    t.integer "round_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "grade"
    t.index ["round_id"], name: "index_fixtures_on_round_id"
  end

  create_table "innings", force: :cascade do |t|
    t.integer "inning_no"
    t.string "batting_team"
    t.string "bowling_team"
    t.integer "runs"
    t.integer "wickets"
    t.integer "byes"
    t.integer "legbyes"
    t.integer "no_balls"
    t.integer "wides"
    t.string "score"
    t.integer "match_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["match_id"], name: "index_innings_on_match_id"
  end

  create_table "keywords", force: :cascade do |t|
    t.string "key_type"
    t.string "key_subtype"
    t.string "key_value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "match_players", force: :cascade do |t|
    t.string "name"
    t.string "team"
    t.integer "player_id"
    t.integer "match_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "position"
    t.index ["match_id"], name: "index_match_players_on_match_id"
  end

  create_table "matches", force: :cascade do |t|
    t.integer "fixture_id"
    t.string "referee1"
    t.string "referee2"
    t.string "scorer"
    t.string "result"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "player_home_1"
    t.integer "player_home_id_1"
    t.string "player_home_2"
    t.integer "player_home_id_2"
    t.string "player_home_3"
    t.integer "player_home_id_3"
    t.string "player_home_4"
    t.integer "player_home_id_4"
    t.string "player_home_5"
    t.integer "player_home_id_5"
    t.string "player_home_6"
    t.integer "player_home_id_6"
    t.string "player_home_7"
    t.integer "player_home_id_7"
    t.string "player_home_8"
    t.integer "player_home_id_8"
    t.string "player_home_9"
    t.integer "player_home_id_9"
    t.string "player_home_10"
    t.integer "player_home_id_10"
    t.string "player_home_11"
    t.integer "player_home_id_11"
    t.string "player_home_12"
    t.integer "player_home_id_12"
    t.string "player_home_13"
    t.integer "player_home_id_13"
    t.string "player_away_1"
    t.integer "player_away_id_1"
    t.string "player_away_2"
    t.integer "player_away_id_2"
    t.string "player_away_3"
    t.integer "player_away_id_3"
    t.string "player_away_4"
    t.integer "player_away_id_4"
    t.string "player_away_5"
    t.integer "player_away_id_5"
    t.string "player_away_6"
    t.integer "player_away_id_6"
    t.string "player_away_7"
    t.integer "player_away_id_7"
    t.string "player_away_8"
    t.integer "player_away_id_8"
    t.string "player_away_9"
    t.integer "player_away_id_9"
    t.string "player_away_10"
    t.integer "player_away_id_10"
    t.string "player_away_11"
    t.integer "player_away_id_11"
    t.string "player_away_12"
    t.integer "player_away_id_12"
    t.string "player_away_13"
    t.integer "player_away_id_13"
    t.index ["fixture_id"], name: "index_matches_on_fixture_id"
  end

  create_table "players", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "full_name"
    t.string "display_name"
    t.string "phone"
    t.string "email"
    t.date "birth_date"
    t.boolean "club_member"
    t.date "date_joined"
    t.integer "club_id"
    t.integer "team_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["club_id"], name: "index_players_on_club_id"
    t.index ["team_id"], name: "index_players_on_team_id"
  end

  create_table "rounds", force: :cascade do |t|
    t.string "round_no"
    t.date "start_date"
    t.date "end_date"
    t.integer "competition_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["competition_id"], name: "index_rounds_on_competition_id"
  end

  create_table "teams", force: :cascade do |t|
    t.string "name"
    t.string "grade"
    t.string "captain"
    t.string "vice_captain"
    t.integer "club_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["club_id"], name: "index_teams_on_club_id"
  end

end
