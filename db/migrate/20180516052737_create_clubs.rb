class CreateClubs < ActiveRecord::Migration[5.1]
  def change
    create_table :clubs do |t|
      t.string :name
      t.string :street_address
      t.string :street_city
      t.string :street_state
      t.string :street_postcode
      t.string :postal_address
      t.string :postal_city
      t.string :postal_state
      t.string :postal_postcode
      t.string :phone
      t.string :fax
      t.string :email
      t.string :website
      t.string :president_name
      t.string :president_contact
      t.string :chairman_name
      t.string :chairman_contact
      t.string :secretary_name
      t.string :secretary_contact
      t.string :treasurer_name
      t.string :treasurer_contact

      t.timestamps
    end
  end
end
