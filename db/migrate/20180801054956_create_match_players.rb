class CreateMatchPlayers < ActiveRecord::Migration[5.1]
  def change
    create_table :match_players do |t|
      t.string :name
      t.string :team
      t.integer :player_id
      t.references :match, foreign_key: true

      t.timestamps
    end
  end
end
