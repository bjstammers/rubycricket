class AddCompOverLimits < ActiveRecord::Migration[5.1]
  def change
    add_column :competitions, :max_overs, :integer
    add_column :competitions, :max_bowler_overs, :integer
  end
end
