class CreateCompetitions < ActiveRecord::Migration[5.1]
  def change
    create_table :competitions do |t|
      t.string :name
      t.date :start_date
      t.date :end_date
      t.integer :match_days
      t.boolean :limited_overs
      t.string :restrict_bowl
      t.string :restrict_bat
      t.string :restrict_field
      t.float :points_win
      t.float :points_draw
      t.float :points_lose
      t.boolean :allow_bonus
      t.string :bonus_criteria
      t.float :points_bonus

      t.timestamps
    end
  end
end
