class CreateInnings < ActiveRecord::Migration[5.1]
  def change
    create_table :innings do |t|
      t.integer :inning_no
      t.string :batting_team
      t.string :bowling_team
      t.integer :runs
      t.integer :wickets
      t.integer :byes
      t.integer :legbyes
      t.integer :no_balls
      t.integer :wides
      t.string :score
      t.references :match, foreign_key: true

      t.timestamps
    end
  end
end
