class CreateTeams < ActiveRecord::Migration[5.1]
  def change
    create_table :teams do |t|
      t.string :name
      t.string :grade
      t.string :captain
      t.string :vice_captain
      t.references :club, foreign_key: true

      t.timestamps
    end
  end
end
