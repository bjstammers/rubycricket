class RoundFixtureLink < ActiveRecord::Migration[5.1]
  def change
    rename_column :fixtures, :comp_round_id, :round_id
  end
end
