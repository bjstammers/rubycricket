class CreatePlayers < ActiveRecord::Migration[5.1]
  def change
    create_table :players do |t|
      t.string :first_name
      t.string :last_name
      t.string :full_name
      t.string :display_name
      t.string :phone
      t.string :email
      t.date :birth_date
      t.boolean :club_member
      t.date :date_joined
      t.references :club, foreign_key: true
      t.references :team, foreign_key: true

      t.timestamps
    end
  end
end
