class CreateMatches < ActiveRecord::Migration[5.1]
  def change
    create_table :matches do |t|
      t.references :fixture, foreign_key: true
      t.string :referee1
      t.string :referee2
      t.string :scorer
      t.string :result

      t.timestamps
    end
  end
end
