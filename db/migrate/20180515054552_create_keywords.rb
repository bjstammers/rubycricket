class CreateKeywords < ActiveRecord::Migration[5.1]
  def change
    create_table :keywords do |t|
      t.string :key_type
      t.string :key_subtype
      t.string :key_value

      t.timestamps
    end
  end
end
