class CreateFixtures < ActiveRecord::Migration[5.1]
  def change
    create_table :fixtures do |t|
      t.string :home_team
      t.string :away_team
      t.string :ground
      t.references :comp_round, foreign_key: true

      t.timestamps
    end
  end
end
