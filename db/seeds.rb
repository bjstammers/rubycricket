# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Keyword.create(key_type: "State", key_subtype: "", key_value: "ACT")
Keyword.create(key_type: "State", key_subtype: "", key_value: "NSW")
Keyword.create(key_type: "State", key_subtype: "", key_value: "NT")
Keyword.create(key_type: "State", key_subtype: "", key_value: "QLD")
Keyword.create(key_type: "State", key_subtype: "", key_value: "SA")
Keyword.create(key_type: "State", key_subtype: "", key_value: "TAS")
Keyword.create(key_type: "State", key_subtype: "", key_value: "VIC")
Keyword.create(key_type: "State", key_subtype: "", key_value: "WA")
