FactoryBot.define do
  factory :player do
    first_name "MyString"
    last_name "MyString"
    full_name "MyString"
    display_name "MyString"
    phone "MyString"
    email "MyString"
    birth_date "2018-05-18"
    club_member false
    date_joined "2018-05-18"
    #club nil
    #team nil

    association :club, factory: :assoc_club
    association :team, factory: :assoc_team
  end

  factory :invalid_player_first_name, parent: :player  do
    first_name nil
  end
  factory :invalid_player_last_name, parent: :player  do
    last_name nil
  end
  factory :invalid_player_full_name, parent: :player  do
    full_name nil
  end
  factory :invalid_player_display_name, parent: :player  do
    display_name nil
  end
  factory :invalid_player_phone, parent: :player  do
    phone nil
  end
  factory :invalid_player_email, parent: :player  do
    email nil
  end
end
