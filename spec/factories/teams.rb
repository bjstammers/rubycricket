FactoryBot.define do
  factory :team do
    name "MyString"
    grade "MyString"
    captain "MyString"
    vice_captain "MyString"
    #club nil
    association :club, factory: :assoc_club
  end


  factory :invalid_team_name, parent: :team  do
    name nil
  end
  factory :invalid_grade, parent: :team  do
    grade nil
  end

end
