FactoryBot.define do
  factory :assoc_team, class: Team do
    name "MyString"
    grade "MyString"
    captain "MyString"
    vice_captain "MyString"
    #club nil
    association :club, factory: :assoc_club
  end

end
