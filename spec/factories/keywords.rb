FactoryBot.define do
  factory :keyword do
      key_type 'First'
      key_subtype 'Second'
      key_value 'Sample text'
  end
  factory :state_keyword, parent: :keyword do
      key_type 'State'
      key_subtype ''
      key_value 'ACT'
  end
  factory :invalid_key_type, parent: :keyword  do
      key_type nil
  end
  factory :invalid_key_value, parent: :keyword  do
      key_value nil
  end
end
