FactoryBot.define do
  factory :fixture do
    home_team "MyString"
    away_team "MyString"
    ground "MyString"
    # round nil
    association :round, factory: :assoc_round
  end
end
