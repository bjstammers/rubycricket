FactoryBot.define do
  factory :assoc_fixture, class: Fixture do
    home_team "MyString"
    away_team "MyString"
    ground "MyString"
    # round nil
    association :round, factory: :assoc_round
  end
end
