FactoryBot.define do
  factory :match do
    fixture nil
    referee1 "MyString"
    referee2 "MyString"
    scorer "MyString"
    result "MyString"
  end
end
