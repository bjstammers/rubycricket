FactoryBot.define do
  factory :assoc_competition, class: Competition do
    name "MyString"
    start_date "2018-06-11"
    end_date "2018-06-11"
    match_days 1
    limited_overs false
    restrict_bowl "MyString"
    restrict_bat "MyString"
    restrict_field "MyString"
    points_win 1.5
    points_draw 1.5
    points_lose 1.5
    allow_bonus false
    bonus_criteria "MyString"
    points_bonus 1.5
  end
end
