FactoryBot.define do
  factory :competition do
    name "MyString"
    start_date "2018-06-11"
    end_date "2018-06-11"
    match_days 1
    limited_overs false
    restrict_bowl "MyString"
    restrict_bat "MyString"
    restrict_field "MyString"
    points_win 1.5
    points_draw 1.5
    points_lose 1.5
    allow_bonus false
    bonus_criteria "MyString"
    points_bonus 1.5
  end
  
  factory :invalid_comp_name, parent: :competition  do
    name nil
  end
  factory :invalid_comp_start_date, parent: :competition  do
    start_date nil
  end
  factory :invalid_comp_end_date, parent: :competition  do
    end_date nil
  end
  factory :invalid_comp_match_days, parent: :competition  do
    match_days nil
  end

end
