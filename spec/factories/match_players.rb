FactoryBot.define do
  factory :match_player do
    name "MyString"
    team "MyString"
    player_id 1
    match nil
  end
end
