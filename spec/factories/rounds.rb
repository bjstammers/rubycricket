FactoryBot.define do
  factory :round do
    round_no "MyString"
    start_date "2018-06-14"
    end_date "2018-06-14"
    # competition nil
    association :competition, factory: :assoc_competition
  end
  
  factory :invalid_round_no, parent: :round  do
    round_no nil
  end
  factory :invalid_round_start_date, parent: :round  do
    start_date nil
  end
  factory :invalid_round_end_date, parent: :round  do
    end_date nil
  end

end
