FactoryBot.define do
  factory :assoc_round, class: Round do
    round_no "MyString"
    start_date "2018-06-14"
    end_date "2018-06-14"
    # competition nil
    association :competition, factory: :assoc_competition
  end
end
