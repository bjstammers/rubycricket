FactoryBot.define do
  factory :club do
    name "MyString"
    street_address "MyString"
    street_city "MyString"
    street_state "MyString"
    street_postcode "MyString"
    postal_address "MyString"
    postal_city "MyString"
    postal_state "MyString"
    postal_postcode "MyString"
    phone "MyString"
    fax "MyString"
    email "MyString"
    website "MyString"
    president_name "MyString"
    president_contact "MyString"
    chairman_name "MyString"
    chairman_contact "MyString"
    secretary_name "MyString"
    secretary_contact "MyString"
    treasurer_name "MyString"
    treasurer_contact "MyString"
  end
  
  factory :club_state_keyword, parent: :keyword do
      key_type 'State'
      key_subtype ''
      key_value 'ACT'
  end
  factory :invalid_club_name, parent: :club  do
    name nil
  end
  factory :invalid_club_street_address, parent: :club  do
    street_address nil
  end
  factory :invalid_club_street_city, parent: :club  do
    street_city nil
  end
  factory :invalid_club_street_state, parent: :club  do
    street_state nil
  end
  factory :invalid_club_phone, parent: :club  do
    phone nil
  end
  factory :invalid_club_email, parent: :club  do
    email nil
  end

end
