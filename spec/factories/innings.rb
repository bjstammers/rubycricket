FactoryBot.define do
  factory :inning do
    inning_no 1
    batting_team "MyString"
    bowling_team "MyString"
    runs 1
    wickets 1
    byes 1
    legbyes 1
    no_balls 1
    wides 1
    score "MyString"
    match nil
  end
end
