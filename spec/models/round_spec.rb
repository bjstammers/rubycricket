require 'rails_helper'

RSpec.describe Round, type: :model do
    context "validation" do
        it {should validate_presence_of :round_no}
        it {should validate_presence_of :start_date}
        it {should validate_presence_of :end_date}
    end

    context "relationships" do
        it {should belong_to :competition}
    end
end
