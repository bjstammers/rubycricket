require 'rails_helper'

  describe Keyword do
      context "validation" do
         it {should validate_presence_of :key_type}
         it {should validate_presence_of :key_value}
      end
  end
