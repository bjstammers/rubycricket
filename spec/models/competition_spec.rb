require 'rails_helper'

describe Competition do
    context "validation" do
        it {should validate_presence_of :name}
        it {should validate_presence_of :start_date}
        it {should validate_presence_of :end_date}
        it {should validate_presence_of :match_days}
    end

    context "relationships" do
        it {should have_many :rounds}
    end
end
