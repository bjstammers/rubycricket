require 'rails_helper'

describe Player, type: :model do
  context "validation" do
      it {should validate_presence_of :first_name}
      it {should validate_presence_of :last_name}
      it {should validate_presence_of :full_name}
      it {should validate_presence_of :display_name}
      it {should validate_presence_of :phone}
      it {should validate_presence_of :email}
  end

  context "relationships" do
      it {should belong_to :club}
      it {should belong_to :team}
  end
end
