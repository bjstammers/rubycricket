require 'rails_helper'

describe Club, type: :model do
    context "validation" do
        it {should validate_presence_of :name}
        it {should validate_presence_of :street_address}
        it {should validate_presence_of :street_city}
        it {should validate_presence_of :street_state}
        it {should validate_presence_of :phone}
        it {should validate_presence_of :email}
    end

    context "relationships" do
        it {should have_many :teams}
        it {should have_many :players}
    end
end
