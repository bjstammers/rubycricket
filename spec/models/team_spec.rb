require 'rails_helper'

describe Team, type: :model do
  context "validation" do
      it {should validate_presence_of :name}
      it {should validate_presence_of :grade}
  end

  context "relationships" do
      it {should belong_to :club}
  end
end
