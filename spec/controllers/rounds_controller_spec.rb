require 'rails_helper'

describe RoundsController do

  before :each do
    @round = create(:round)
  end

  describe "GET #index" do
    it "returns index of all rounds" do
      get :index
      should render_template(:index)
    end
  end

  describe "GET #show" do
    it "returns show round" do
      get :show, params: {id: @round.id }
      should render_template(:show)
    end
  end

  describe "GET #new" do
    it "returns the new round form" do
      get :new
      should render_template(:new)
    end
  end

  describe "POST #create" do
    context "with valid attributes" do
      it "create a new round successfully" do
        expect {
          post :create, params: {round: attributes_for(:round) }
        }.to change{Round.count}.by(1)
      end
      it "opens the new competition" do
        post :create, params: {round: attributes_for(:round)}
        should redirect_to(Round.last)
      end
    end

    context "with invalid attributes" do
      it "does not create a new round without round no" do
        expect {
          post :create, params: {round: FactoryBot.attributes_for(:invalid_round_no)}
        }.to_not change{Round.count}
      end
      it "re-opens the new round form for an invalid round no" do
        post :create, params: {round: FactoryBot.attributes_for(:invalid_round_no)}
        should render_template(:new)
      end
      it "does not create a new round without start_date" do
        expect {
          post :create, params: {round: FactoryBot.attributes_for(:invalid_round_start_date)}
        }.to_not change{Round.count}
      end
      it "re-opens the new round form for an invalid start date" do
        post :create, params: {round: FactoryBot.attributes_for(:invalid_round_start_date)}
        should render_template(:new)
      end
      it "does not create a new round without end_date" do
        expect {
          post :create, params: {round: FactoryBot.attributes_for(:invalid_round_end_date)}
        }.to_not change{Round.count}
      end
      it "re-opens the new round form for an invalid end date" do
        post :create, params: {round: FactoryBot.attributes_for(:invalid_round_end_date)}
        should render_template(:new)
      end
    end
  end

  # describe "GET #edit" do
  #   it "should open the competition edit form" do
  #     get :edit, params: {id: @round.id }
  #     should render_template(:edit)
  #   end
  # end

  # describe "PUT #update" do
  #   context "with valid attributes" do
  #     before :each do
  #       @test_comp = create(:round)
  #       put :update, params: {id: @test_comp, round: FactoryBot.attributes_for(:round, name: "Comp 01", start_date: "01/01/2018", end_date: "01/07/2018", match_days: 3)}
  #       @test_comp.reload
  #     end
  #     it "should update the name" do
  #       @test_comp.name.should eq("Comp 01")
  #     end
  #     it "should update the start_date" do
  #       @test_comp.start_date.iso8601.should eq("2018-01-01")
  #     end
  #     it "should update the end_date" do
  #       @test_comp.end_date.iso8601.should eq("2018-07-01")
  #     end
  #     it "should update the match_days" do
  #       @test_comp.match_days.should eq(3)
  #     end
  #     it "opens the updated competition" do
  #       put :update, params: {id: @test_comp, round: FactoryBot.attributes_for(:round, name: "Comp 01", start_date: "01/01/2018", end_date: "01/07/2018", match_days: 3)}
  #       should redirect_to(@test_comp)
  #     end
  #   end

  #   context "with invalid attributes" do
  #     before :each do
  #       @test_comp = create(:round)
  #       put :update, params: {id: @test_comp, round: FactoryBot.attributes_for(:round, name: nil, start_date: nil, end_date: nil, match_days: nil)}
  #       @test_comp.reload
  #     end
  #     it "should not update the name" do
  #       @test_comp.name.should eq(@round.name)
  #     end
  #     it "should not update the start_date" do
  #       @test_comp.start_date.rfc2822.should eq(@round.start_date.rfc2822)
  #     end
  #     it "should not update the end_date" do
  #       @test_comp.end_date.rfc2822.should eq(@round.end_date.rfc2822)
  #     end
  #     it "should not update the match_days" do
  #       @test_comp.match_days.should eq(@round.match_days)
  #     end
  #     it "re-opens the edit page" do
  #       put :update, params: {id: @test_comp, round: FactoryBot.attributes_for(:round, name: nil, start_date: nil, end_date: nil, match_days: nil)}
  #       should render_template(:edit)
  #     end
  #   end
  # end

  # describe "DELETE #destroy" do
  #   it "deletes the keyword" do
  #     @round = FactoryBot.create(:round)
  #     expect {
  #       delete :destroy, params: { id: @round.id }
  #     }.to change{Competition.count}.by(-1)
  #   end
  #   it "opens the keyword index page" do
  #     @round = FactoryBot.create(:round)
  #     delete :destroy, params: { id: @round.id }
  #     should redirect_to competitions_url
  #   end
  # end
end
