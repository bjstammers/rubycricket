require 'rails_helper'

describe MatchesController do
  before :each do
    @match = create(:match)
  end

  describe "GET #index" do
    it "returns index of all matches" do
      get :index
      should render_template(:index)
    end
  end

  describe "GET #show" do
    it "returns show competition" do
      get :show, params: {id: @match.id }
      should render_template(:show)
    end
  end

  describe "GET #new" do
    it "returns the new match form" do
      get :new
      should render_template(:new)
    end
  end

  describe "POST #create" do
    context "with valid attributes" do
      it "create a new match successfully" do
        expect {
          post :create, params: {match: attributes_for(:match) }
        }.to change{Match.count}.by(1)
      end
      it "opens the new match" do
        post :create, params: {match: attributes_for(:match)}
        should redirect_to(Match.last)
      end
    end

    # context "with invalid attributes" do
    #   it "does not create a new match without name" do
    #     expect {
    #       post :create, params: {match: FactoryBot.attributes_for(:invalid_comp_name)}
    #     }.to_not change{Match.count}
    #   end
    #   it "re-opens the new competition form for an invalid name" do
    #     post :create, params: {match: FactoryBot.attributes_for(:invalid_comp_name)}
    #     should render_template(:new)
    #   end
    #   it "does not create a new competition without start_date" do
    #     expect {
    #       post :create, params: {match: FactoryBot.attributes_for(:invalid_comp_start_date)}
    #     }.to_not change{Competition.count}
    #   end
    #   it "re-opens the new competition form for an invalid start date" do
    #     post :create, params: {match: FactoryBot.attributes_for(:invalid_comp_start_date)}
    #     should render_template(:new)
    #   end
    #   it "does not create a new competition without end_date" do
    #     expect {
    #       post :create, params: {match: FactoryBot.attributes_for(:invalid_comp_end_date)}
    #     }.to_not change{Competition.count}
    #   end
    #   it "re-opens the new club form for an invalid end date" do
    #     post :create, params: {match: FactoryBot.attributes_for(:invalid_comp_end_date)}
    #     should render_template(:new)
    #   end
    #   it "does not create a new club without match_days" do
    #     expect {
    #       post :create, params: {match: FactoryBot.attributes_for(:invalid_comp_match_days)}
    #     }.to_not change{Competition.count}
    #   end
    #   it "re-opens the new club form for an invalid match days" do
    #     post :create, params: {match: FactoryBot.attributes_for(:invalid_comp_match_days)}
    #     should render_template(:new)
    #   end
    # end
  end

  describe "GET #edit" do
    it "should open the match edit form" do
      get :edit, params: {id: @match.id }
      should render_template(:edit)
    end
  end

  describe "PUT #update" do
    context "with valid attributes" do
      before :each do
        @test_match = create(:match)
        put :update, params: {id: @test_match, match: FactoryBot.attributes_for(:match, referee1: "Ref 01", referee02: "Ref 02", scorer: "Joe Bloggs", result: "Won by an innings and 53 runs")}
        @test_match.reload
      end
      it "should update referee1" do
        @test_match.referee1.should eq("Ref 01")
      end
      it "should update referee2" do
        @test_match.referee2.should eq("Ref 02")
      end
      it "should update the scorer" do
        @test_match.scorer.should eq("Joe Bloggs")
      end
      it "should update the result" do
        @test_match.result.should eq("Won by an innings and 53 runs")
      end
      it "opens the updated competition" do
        put :update, params: {id: @test_match, match: FactoryBot.attributes_for(:match, referee1: "Ref 01", referee02: "Ref 02", scorer: "Joe Bloggs", result: "Won by an innings and 53 runs")}
        should redirect_to(@test_match)
      end
    end

    # context "with invalid attributes" do
    #   before :each do
    #     @test_match = create(:match)
    #     put :update, params: {id: @test_match, match: FactoryBot.attributes_for(:match, name: nil, start_date: nil, end_date: nil, match_days: nil)}
    #     @test_match.reload
    #   end
    #   it "should not update the name" do
    #     @test_match.name.should eq(@match.name)
    #   end
    #   it "should not update the start_date" do
    #     @test_match.start_date.rfc2822.should eq(@match.start_date.rfc2822)
    #   end
    #   it "should not update the end_date" do
    #     @test_match.end_date.rfc2822.should eq(@match.end_date.rfc2822)
    #   end
    #   it "should not update the match_days" do
    #     @test_match.match_days.should eq(@match.match_days)
    #   end
    #   it "re-opens the edit page" do
    #     put :update, params: {id: @test_match, match: FactoryBot.attributes_for(:match, name: nil, start_date: nil, end_date: nil, match_days: nil)}
    #     should render_template(:edit)
    #   end
    # end
  end

  describe "DELETE #destroy" do
    it "deletes the keyword" do
      @match = FactoryBot.create(:match)
      expect {
        delete :destroy, params: { id: @match.id }
      }.to change{Match.count}.by(-1)
    end
    it "opens the keyword index page" do
      @match = FactoryBot.create(:match)
      delete :destroy, params: { id: @match.id }
      should redirect_to matches_url
    end
  end

end
