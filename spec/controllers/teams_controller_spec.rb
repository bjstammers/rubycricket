require 'rails_helper'

describe TeamsController, type: :controller do

  before :each do
    @team = create(:team)
  end

  describe "GET #index" do
    it "returns index of all teams" do
      get :index
      should render_template(:index)
    end
  end

  describe "GET #show" do
    it "returns show team" do
      get :show, params: {id: @team.id }
      should render_template(:show)
    end
  end

  describe "GET #new" do
    it "returns the new team form" do
      get :new
      should render_template(:new)
    end
  end

   describe "POST #create" do
     context "with valid attributes" do
       it "create a new team successfully" do
         expect {
           post :create, params: {team: attributes_for(:team) }
         }.to change{Team.count}.by(1)
       end
       it "opens the new team" do
         post :create, params: {team: attributes_for(:team)}
         should redirect_to(Team.last)
       end
     end

    context "with invalid attributes" do
      it "does not create a new team without name" do
        expect {
          post :create, params: {team: FactoryBot.attributes_for(:invalid_team_name)}
        }.to_not change{Team.count}
      end
      it "re-opens the new team form for an invalid name" do
        post :create, params: {team: FactoryBot.attributes_for(:invalid_team_name)}
        should render_template(:new)
      end
      it "does not create a new team without grade" do
        expect {
          post :create, params: {team: FactoryBot.attributes_for(:invalid_grade)}
        }.to_not change{Team.count}
      end
      it "re-opens the new club form for an invalid grade" do
        post :create, params: {team: FactoryBot.attributes_for(:invalid_grade)}
        should render_template(:new)
      end
    end
   end

  describe "GET #edit" do
    it "should open the team edit form" do
      get :edit, params: {id: @team.id }
      should render_template(:edit)
    end
  end

  describe "PUT #update" do
    context "with valid attributes" do
      before :each do
        @test_team = create(:team)
        put :update, params: {id: @test_team, team: FactoryBot.attributes_for(:team, name: "Next Team", grade: "First Grade", captain: "Fred Smith", vice_captain: "Joe Bloggs")}
        @test_team.reload
      end
      it "should update the name" do
        @test_team.name.should eq("Next Team")
      end
      it "should update the grade" do
        @test_team.grade.should eq("First Grade")
      end
      it "should update the captain" do
        @test_team.captain.should eq("Fred Smith")
      end
      it "should update the vice_captain" do
        @test_team.vice_captain.should eq("Joe Bloggs")
      end
      it "opens the updated team" do
        put :update, params: {id: @test_team, team: FactoryBot.attributes_for(:team, name: "Next Team", grade: "First Grade", captain: "Fred Smith", vice_captain: "Joe Bloggs")}
        should redirect_to(@test_team)
      end
    end

    context "with invalid attributes" do
      before :each do
        @test_team = create(:team)
        put :update, params: {id: @test_team, team: FactoryBot.attributes_for(:team, name: nil, grade: nil, captain: "Fred Smith", vice_captain: "Joe Bloggs")}
        @test_team.reload
      end
      it "should not update the name" do
        @test_team.name.should eq(@team.name)
      end
      it "should not update the grade" do
        @test_team.grade.should eq(@team.grade)
      end
      it "should not update the captain" do
        @test_team.captain.should eq(@team.captain)
      end
      it "should not update the vice_captain" do
        @test_team.vice_captain.should eq(@team.vice_captain)
      end
      it "re-opens the edit page" do
        put :update, params: {id: @test_team, team: FactoryBot.attributes_for(:team, name: nil, grade: nil, captain: "Fred Smith", vice_captain: "Joe Bloggs")}
        should render_template(:edit)
      end
    end
  end

  describe "DELETE #destroy" do
    it "deletes the keyword" do
      @club = FactoryBot.create(:team)
      expect {
        delete :destroy, params: { id: @team.id }
      }.to change{Team.count}.by(-1)
    end
    it "opens the keyword index page" do
      @club = FactoryBot.create(:team)
      delete :destroy, params: { id: @team.id }
      should redirect_to teams_url
    end
  end

end
