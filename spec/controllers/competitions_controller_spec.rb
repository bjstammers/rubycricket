require 'rails_helper'

describe CompetitionsController do

  before :each do
    @competition = create(:competition)
  end

  describe "GET #index" do
    it "returns index of all competitions" do
      get :index
      should render_template(:index)
    end
  end

  describe "GET #show" do
    it "returns show competition" do
      get :show, params: {id: @competition.id }
      should render_template(:show)
    end
  end

  describe "GET #new" do
    it "returns the new competition form" do
      get :new
      should render_template(:new)
    end
  end

  describe "POST #create" do
    context "with valid attributes" do
      it "create a new competition successfully" do
        expect {
          post :create, params: {competition: attributes_for(:competition) }
        }.to change{Competition.count}.by(1)
      end
      it "opens the new competition" do
        post :create, params: {competition: attributes_for(:competition)}
        should redirect_to(Competition.last)
      end
    end

    context "with invalid attributes" do
      it "does not create a new competition without name" do
        expect {
          post :create, params: {competition: FactoryBot.attributes_for(:invalid_comp_name)}
        }.to_not change{Competition.count}
      end
      it "re-opens the new competition form for an invalid name" do
        post :create, params: {competition: FactoryBot.attributes_for(:invalid_comp_name)}
        should render_template(:new)
      end
      it "does not create a new competition without start_date" do
        expect {
          post :create, params: {competition: FactoryBot.attributes_for(:invalid_comp_start_date)}
        }.to_not change{Competition.count}
      end
      it "re-opens the new competition form for an invalid start date" do
        post :create, params: {competition: FactoryBot.attributes_for(:invalid_comp_start_date)}
        should render_template(:new)
      end
      it "does not create a new competition without end_date" do
        expect {
          post :create, params: {competition: FactoryBot.attributes_for(:invalid_comp_end_date)}
        }.to_not change{Competition.count}
      end
      it "re-opens the new club form for an invalid end date" do
        post :create, params: {competition: FactoryBot.attributes_for(:invalid_comp_end_date)}
        should render_template(:new)
      end
      it "does not create a new club without match_days" do
        expect {
          post :create, params: {competition: FactoryBot.attributes_for(:invalid_comp_match_days)}
        }.to_not change{Competition.count}
      end
      it "re-opens the new club form for an invalid match days" do
        post :create, params: {competition: FactoryBot.attributes_for(:invalid_comp_match_days)}
        should render_template(:new)
      end
    end
  end

  describe "GET #edit" do
    it "should open the competition edit form" do
      get :edit, params: {id: @competition.id }
      should render_template(:edit)
    end
  end

  describe "PUT #update" do
    context "with valid attributes" do
      before :each do
        @test_comp = create(:competition)
        put :update, params: {id: @test_comp, competition: FactoryBot.attributes_for(:competition, name: "Comp 01", start_date: "01/01/2018", end_date: "01/07/2018", match_days: 3)}
        @test_comp.reload
      end
      it "should update the name" do
        @test_comp.name.should eq("Comp 01")
      end
      it "should update the start_date" do
        @test_comp.start_date.iso8601.should eq("2018-01-01")
      end
      it "should update the end_date" do
        @test_comp.end_date.iso8601.should eq("2018-07-01")
      end
      it "should update the match_days" do
        @test_comp.match_days.should eq(3)
      end
      it "opens the updated competition" do
        put :update, params: {id: @test_comp, competition: FactoryBot.attributes_for(:competition, name: "Comp 01", start_date: "01/01/2018", end_date: "01/07/2018", match_days: 3)}
        should redirect_to(@test_comp)
      end
    end

    context "with invalid attributes" do
      before :each do
        @test_comp = create(:competition)
        put :update, params: {id: @test_comp, competition: FactoryBot.attributes_for(:competition, name: nil, start_date: nil, end_date: nil, match_days: nil)}
        @test_comp.reload
      end
      it "should not update the name" do
        @test_comp.name.should eq(@competition.name)
      end
      it "should not update the start_date" do
        @test_comp.start_date.rfc2822.should eq(@competition.start_date.rfc2822)
      end
      it "should not update the end_date" do
        @test_comp.end_date.rfc2822.should eq(@competition.end_date.rfc2822)
      end
      it "should not update the match_days" do
        @test_comp.match_days.should eq(@competition.match_days)
      end
      it "re-opens the edit page" do
        put :update, params: {id: @test_comp, competition: FactoryBot.attributes_for(:competition, name: nil, start_date: nil, end_date: nil, match_days: nil)}
        should render_template(:edit)
      end
    end
  end

  describe "DELETE #destroy" do
    it "deletes the keyword" do
      @competition = FactoryBot.create(:competition)
      expect {
        delete :destroy, params: { id: @competition.id }
      }.to change{Competition.count}.by(-1)
    end
    it "opens the keyword index page" do
      @competition = FactoryBot.create(:competition)
      delete :destroy, params: { id: @competition.id }
      should redirect_to competitions_url
    end
  end

end
