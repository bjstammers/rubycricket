require 'rails_helper'

describe ClubsController do

  before :each do
    @club = create(:club)
  end

  describe "GET #index" do
    it "returns index of all clubs" do
      get :index
      should render_template(:index)
    end
  end

  describe "GET #show" do
    it "returns show club" do
      get :show, params: {id: @club.id }
      should render_template(:show)
    end
  end

  describe "GET #new" do
    it "returns the new club form" do
      get :new
      should render_template(:new)
    end
  end

   describe "POST #create" do
     context "with valid attributes" do
       it "create a new club successfully" do
         expect {
           post :create, params: {club: attributes_for(:club) }
         }.to change{Club.count}.by(1)
       end
       it "opens the new club" do
         post :create, params: {club: attributes_for(:club)}
         should redirect_to(Club.last)
       end
     end

    context "with invalid attributes" do
      it "does not create a new club without name" do
        expect {
          post :create, params: {club: FactoryBot.attributes_for(:invalid_club_name)}
        }.to_not change{Club.count}
      end
      it "re-opens the new club form for an invalid name" do
        post :create, params: {club: FactoryBot.attributes_for(:invalid_club_name)}
        should render_template(:new)
      end
      it "does not create a new club without street_address" do
        expect {
          post :create, params: {club: FactoryBot.attributes_for(:invalid_club_street_address)}
        }.to_not change{Keyword.count}
      end
      it "re-opens the new club form for an invalid street address" do
        post :create, params: {club: FactoryBot.attributes_for(:invalid_club_street_address)}
        should render_template(:new)
      end
      it "does not create a new club without street_city" do
        expect {
          post :create, params: {club: FactoryBot.attributes_for(:invalid_club_street_city)}
        }.to_not change{Keyword.count}
      end
      it "re-opens the new club form for an invalid street city" do
        post :create, params: {club: FactoryBot.attributes_for(:invalid_club_street_city)}
        should render_template(:new)
      end
      it "does not create a new club without street_state" do
        expect {
          post :create, params: {club: FactoryBot.attributes_for(:invalid_club_street_state)}
        }.to_not change{Keyword.count}
      end
      it "re-opens the new club form for an invalid street state" do
        post :create, params: {club: FactoryBot.attributes_for(:invalid_club_street_state)}
        should render_template(:new)
      end
      it "does not create a new club without phone" do
        expect {
          post :create, params: {club: FactoryBot.attributes_for(:invalid_club_phone)}
        }.to_not change{Keyword.count}
      end
      it "re-opens the new club form for an invalid phone" do
        post :create, params: {club: FactoryBot.attributes_for(:invalid_club_phone)}
        should render_template(:new)
      end
      it "does not create a new club without email" do
        expect {
          post :create, params: {club: FactoryBot.attributes_for(:invalid_club_email)}
        }.to_not change{Club.count}
      end
      it "re-opens the new club form for an invalid email" do
        post :create, params: {club: FactoryBot.attributes_for(:invalid_club_email)}
        should render_template(:new)
      end
    end
   end

  describe "GET #edit" do
    it "should open the club edit form" do
      get :edit, params: {id: @club.id }
      should render_template(:edit)
    end
  end

  describe "PUT #update" do
    context "with valid attributes" do
      before :each do
        @test_club = create(:club)
        put :update, params: {id: @test_club, club: FactoryBot.attributes_for(:club, name: "Example 1", street_address: "Example 2", street_city: "Example 3", street_state: "QLD", postal_address: "Example 4", postal_city: "Example 5", postal_state: "QLD", phone: "Example 6", email: "Example 7")}
        @test_club.reload
      end
      it "should update the name" do
        @test_club.name.should eq("Example 1")
      end
      it "should update the street_address" do
        @test_club.street_address.should eq("Example 2")
      end
      it "should update the street_city" do
        @test_club.street_city.should eq("Example 3")
      end
      it "should update the street_state" do
        @test_club.street_state.should eq("QLD")
      end
      it "should update the postal_address" do
        @test_club.postal_address.should eq("Example 4")
      end
      it "should update the postal_city" do
        @test_club.postal_city.should eq("Example 5")
      end
      it "should update the postal_state" do
        @test_club.postal_state.should eq("QLD")
      end
      it "should update the phone" do
        @test_club.phone.should eq("Example 6")
      end
      it "should update the email" do
        @test_club.email.should eq("Example 7")
      end
      it "opens the updated club" do
        put :update, params: {id: @test_club, club: FactoryBot.attributes_for(:club, name: "Example 1", street_address: "Example 2", street_city: "Example 3", street_state: "QLD", postal_address: "Example 4", postal_city: "Example 5", postal_state: "QLD", phone: "Example 6", email: "Example 7")}
        should redirect_to(@test_club)
      end
    end

    context "with invalid attributes" do
      before :each do
        @test_club = create(:club)
        put :update, params: {id: @test_club, club: FactoryBot.attributes_for(:club, name: nil, street_address: nil, street_city: nil, street_state: nil, postal_address: "Example 4", postal_city: "Example 5", postal_state: "QLD", phone: nil, email: nil)}
        @test_club.reload
      end
      it "should not update the name" do
        @test_club.name.should eq(@club.name)
      end
      it "should not update the street_address" do
        @test_club.street_address.should eq(@club.street_address)
      end
      it "should not update the street_city" do
        @test_club.street_city.should eq(@club.street_city)
      end
      it "should not update the street_state" do
        @test_club.street_state.should eq(@club.street_state)
      end
      it "should not update the phone" do
        @test_club.phone.should eq(@club.phone)
      end
      it "should not update the email" do
        @test_club.email.should eq(@club.email)
      end
      it "re-opens the edit page" do
        put :update, params: {id: @test_club, club: FactoryBot.attributes_for(:club, name: nil, street_address: nil, street_city: nil, street_state: nil, postal_address: "Example 4", postal_city: "Example 5", postal_state: "QLD", phone: nil, email: nil)}
        should render_template(:edit)
      end
    end
  end

  describe "DELETE #destroy" do
    it "deletes the keyword" do
      @club = FactoryBot.create(:club)
      expect {
        delete :destroy, params: { id: @club.id }
      }.to change{Club.count}.by(-1)
    end
    it "opens the keyword index page" do
      @club = FactoryBot.create(:club)
      delete :destroy, params: { id: @club.id }
      should redirect_to clubs_url
    end
  end


end
