require 'rails_helper'

describe PlayersController, type: :controller do
  before :each do
    @player = create(:player)
  end

  describe "GET #index" do
    it "returns index of all players" do
      get :index
      should render_template(:index)
    end
  end

  describe "GET #show" do
    it "returns show player" do
      get :show, params: {id: @player.id }
      should render_template(:show)
    end
  end

  describe "GET #new" do
    it "returns the new player form" do
      get :new
      should render_template(:new)
    end
  end

   describe "POST #create" do
     context "with valid attributes" do
       it "create a new player successfully" do
         expect {
           post :create, params: {player: attributes_for(:player) }
         }.to change{Player.count}.by(1)
       end
       it "opens the new player" do
         post :create, params: {player: attributes_for(:player)}
         should redirect_to(Player.last)
       end
     end

    context "with invalid attributes" do
      it "does not create a new player without first_name" do
        expect {
          post :create, params: {player: FactoryBot.attributes_for(:invalid_player_first_name)}
        }.to_not change{Player.count}
      end
      it "re-opens the new player form for an invalid first_name" do
        post :create, params: {player: FactoryBot.attributes_for(:invalid_player_first_name)}
        should render_template(:new)
      end
      it "does not create a new player without last_name" do
        expect {
          post :create, params: {player: FactoryBot.attributes_for(:invalid_player_last_name)}
        }.to_not change{Player.count}
      end
      it "re-opens the new player form for an invalid last_name" do
        post :create, params: {player: FactoryBot.attributes_for(:invalid_player_last_name)}
        should render_template(:new)
      end
      it "does not create a new player without full_name" do
        expect {
          post :create, params: {player: FactoryBot.attributes_for(:invalid_player_full_name)}
        }.to_not change{Player.count}
      end
      it "re-opens the new player form for an invalid full_name" do
        post :create, params: {player: FactoryBot.attributes_for(:invalid_player_full_name)}
        should render_template(:new)
      end
      it "does not create a new player without display_name" do
        expect {
          post :create, params: {player: FactoryBot.attributes_for(:invalid_player_display_name)}
        }.to_not change{Player.count}
      end
      it "re-opens the new player form for an invalid display_name" do
        post :create, params: {player: FactoryBot.attributes_for(:invalid_player_display_name)}
        should render_template(:new)
      end
      it "does not create a new player without phone" do
        expect {
          post :create, params: {player: FactoryBot.attributes_for(:invalid_player_phone)}
        }.to_not change{Player.count}
      end
      it "re-opens the new club form for an invalid phone" do
        post :create, params: {player: FactoryBot.attributes_for(:invalid_player_phone)}
        should render_template(:new)
      end
      it "does not create a new player without email" do
        expect {
          post :create, params: {player: FactoryBot.attributes_for(:invalid_player_email)}
        }.to_not change{Player.count}
      end
      it "re-opens the new club form for an invalid email" do
        post :create, params: {player: FactoryBot.attributes_for(:invalid_player_email)}
        should render_template(:new)
      end
    end
   end

  describe "GET #edit" do
    it "should open the player edit form" do
      get :edit, params: {id: @player.id }
      should render_template(:edit)
    end
  end

  describe "PUT #update" do
    context "with valid attributes" do
      before :each do
        @test_player = create(:player)
        put :update, params: {id: @test_player, player: FactoryBot.attributes_for(:player, first_name: "Joe", last_name: "Bloggs", full_name: "Joe Bloggs", display_name: "J. Bloggs", phone: "33333333", email: "a@b.com", birth_date: "01/01/2010", club_member: true, date_joined: "01/01/2017")}
        @test_player.reload
      end
      it "should update the first_name" do
        @test_player.first_name.should eq("Joe")
      end
      it "should update the last_name" do
        @test_player.last_name.should eq("Bloggs")
      end
      it "should update the full_name" do
        @test_player.full_name.should eq("Joe Bloggs")
      end
      it "should update the display_name" do
        @test_player.display_name.should eq("J. Bloggs")
      end
      it "should update the phone" do
        @test_player.phone.should eq("33333333")
      end
      it "should update the email" do
        @test_player.email.should eq("a@b.com")
      end
      it "should update the birth_date" do
        @test_player.birth_date.should eq("01/01/2010")
      end
      it "should update the club_member" do
        @test_player.club_member.should eq(true)
      end
      it "should update the date_joined" do
        @test_player.date_joined.should eq("01/01/2017")
      end
      it "opens the updated player" do
        put :update, params: {id: @test_player, player: FactoryBot.attributes_for(:player, first_name: "Joe", last_name: "Bloggs", full_name: "Joe Bloggs", display_name: "J. Bloggs", phone: "33333333", email: "a@b.com", birth_date: "01/01/2010", club_member: true, date_joined: "01/01/2017")}
        should redirect_to(@test_player)
      end
    end

    context "with invalid attributes" do
      before :each do
        @test_player = create(:player)
        put :update, params: {id: @test_player, player: FactoryBot.attributes_for(:player, first_name: nil, last_name: nil, full_name: nil, display_name: nil, phone: nil, email: nil, birth_date: "01/01/2010", club_member: true, date_joined: "01/01/2017")}
        @test_player.reload
      end
      it "should not update the first_name" do
        @test_player.first_name.should eq(@player.first_name)
      end
      it "should not update the last_name" do
        @test_player.last_name.should eq(@player.last_name)
      end
      it "should not update the full_name" do
        @test_player.full_name.should eq(@player.full_name)
      end
      it "should not update the display_name" do
        @test_player.display_name.should eq(@player.display_name)
      end
      it "should not update the phone" do
        @test_player.phone.should eq(@player.phone)
      end
      it "should not update the email" do
        @test_player.email.should eq(@player.email)
      end
      it "should not update the birth_date" do
        @test_player.birth_date.should eq(@player.birth_date)
      end
      it "should not update the club_member" do
        @test_player.club_member.should eq(@player.club_member)
      end
      it "should not update the date_joined" do
        @test_player.date_joined.should eq(@player.date_joined)
      end
      it "re-opens the edit page" do
        put :update, params: {id: @test_player, player: FactoryBot.attributes_for(:player, first_name: nil, last_name: nil, full_name: nil, display_name: nil, phone: nil, email: nil, birth_date: "01/01/2010", club_member: true, date_joined: "01/01/2017")}
        should render_template(:edit)
      end
    end
  end

  describe "DELETE #destroy" do
    it "deletes the keyword" do
      @club = FactoryBot.create(:player)
      expect {
        delete :destroy, params: { id: @player.id }
      }.to change{Player.count}.by(-1)
    end
    it "opens the keyword index page" do
      @club = FactoryBot.create(:player)
      delete :destroy, params: { id: @player.id }
      should redirect_to players_url
    end
  end

end
