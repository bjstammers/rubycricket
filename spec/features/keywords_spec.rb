require 'rails_helper'

RSpec.feature "Keywords", type: :feature do

  scenario 'keyword index' do
    visit keywords_path
    expect(page).to have_content('Type')
    expect(page).to have_content('Subtype')
    expect(page).to have_content('Value')
    expect(page).to have_link('New')
  end

  feature 'create keyword' do
    scenario 'check form structure' do
      visit new_keyword_path
      #expect(page).to have_content('Client Details')
      expect(page).to have_button('Save')
      expect(page).to have_field('key_type')
      expect(page).to have_field('key_subtype')
      expect(page).to have_field('key_value')
    end
    scenario 'populate and submit form' do
      visit new_keyword_path
      fill_in 'key_type', with: 'First'
      fill_in 'key_subtype', with: 'Second'
      fill_in 'key_value', with: 'Test value'
      click_button 'Save'
      expect(page).to have_content("success")
      
    end
  end
  scenario 'edit keyword' do
    visit keyword_path(1)
    click_link "Edit"
    fill_in 'key_type', with: 'Type 2'
    fill_in 'key_subtype', with: 'Another subtype'
    fill_in 'key_value', with: 'More test values'
    click_button 'Save'
    expect(page).to have_content("success")
  end

  scenario 'invalid keyword' do
    visit new_keyword_path
    #fill_in 'key_type', with: 'Type 2'
    fill_in 'key_subtype', with: 'Another subtype'
    fill_in 'key_value', with: 'More test values'
    click_button 'Save'
    expect(page).to have_content("error")
  end

end
