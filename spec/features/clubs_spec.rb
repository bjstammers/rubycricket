require 'rails_helper'

RSpec.feature "Clubs", type: :feature do

  scenario 'clubs index' do
    visit clubs_path
    expect(page).to have_content('Club')
    expect(page).to have_content('Address')
    expect(page).to have_content('Phone')
    expect(page).to have_content('Email')
    expect(page).to have_content('Website')
    expect(page).to have_link('New')
  end

  feature 'create club' do
    scenario 'check form structure' do
      visit new_club_path
      #expect(page).to have_content('Client Details')
      expect(page).to have_button('Save')
      expect(page).to have_field('club_name')
      expect(page).to have_field('street_address')
      expect(page).to have_field('street_city')
      expect(page).to have_select('street_state')
      expect(page).to have_field('street_postcode')
      expect(page).to have_field('postal_address')
      expect(page).to have_field('postal_city')
      expect(page).to have_select('postal_state')
      expect(page).to have_field('postal_postcode')
      expect(page).to have_field('phone')
      expect(page).to have_field('fax')
      expect(page).to have_field('email')
      expect(page).to have_field('website')
      expect(page).to have_field('president_name')
      expect(page).to have_field('president_contact')
      expect(page).to have_field('chairman_name')
      expect(page).to have_field('chairman_contact')
      expect(page).to have_field('secretary_name')
      expect(page).to have_field('secretary_contact')
      expect(page).to have_field('treasurer_name')
      expect(page).to have_field('treasurer_contact')
    end
    scenario 'populate and submit form' do
      visit new_club_path
      fill_in 'club_name', with: 'First'
      fill_in 'street_address', with: 'Second'
      fill_in 'street_city', with: 'Third'
      page.select('ACT', from: 'street_state')
      fill_in 'street_postcode', with: 'Fourth'
      fill_in 'postal_address', with: 'Fifth'
      fill_in 'postal_city', with: 'Sixth'
      page.select('ACT', from: 'postal_state')
      fill_in 'postal_postcode', with: 'Seventh'
      fill_in 'phone', with: 'Eighth'
      fill_in 'fax', with: 'Ninth'
      fill_in 'email', with: 'Tenth'
      fill_in 'website', with: 'Eleventh'
      fill_in 'president_name', with: 'Twelfth'
      fill_in 'president_contact', with: 'Thirteenth'
      fill_in 'chairman_name', with: 'Fourteenth'
      fill_in 'chairman_contact', with: 'Fifteenth'
      fill_in 'secretary_name', with: 'Sixteenth'
      fill_in 'secretary_contact', with: 'Seventeenth'
      fill_in 'treasurer_name', with: 'Eighteenth'
      fill_in 'treasurer_contact', with: 'Nineteenth'
      click_button 'Save'
      expect(page).to have_content("success")

    end
  end
   scenario 'edit club' do
      @club = FactoryBot.create(:club)
      visit edit_club_path(1)
      fill_in 'club_name', with: 'First a'
      fill_in 'street_address', with: 'Second a'
      fill_in 'street_city', with: 'Third a'
      page.select('ACT', from: 'street_state')
      fill_in 'street_postcode', with: 'Fourth a'
      fill_in 'postal_address', with: 'Fifth a'
      fill_in 'postal_city', with: 'Sixth a'
      page.select('ACT', from: 'postal_state')
      fill_in 'postal_postcode', with: 'Seventh a'
      fill_in 'phone', with: 'Eighth a'
      fill_in 'fax', with: 'Ninth a'
      fill_in 'email', with: 'Tenth a'
      fill_in 'website', with: 'Eleventh a'
      fill_in 'president_name', with: 'Twelfth a'
      fill_in 'president_contact', with: 'Thirteenth a'
      fill_in 'chairman_name', with: 'Fourteenth a'
      fill_in 'chairman_contact', with: 'Fifteenth a'
      fill_in 'secretary_name', with: 'Sixteenth a'
      fill_in 'secretary_contact', with: 'Seventeenth a'
      fill_in 'treasurer_name', with: 'Eighteenth a'
      fill_in 'treasurer_contact', with: 'Nineteenth a'
      click_button 'Save'
      expect(page).to have_content("success")
   end

  scenario 'invalid club' do
    visit new_club_path
    #fill_in 'club_name', with: 'First'
    #fill_in 'street_address', with: 'Second'
    fill_in 'street_city', with: 'Third'
    page.select('ACT', from: 'street_state')
    fill_in 'street_postcode', with: 'Fourth'
    fill_in 'postal_address', with: 'Fifth'
    fill_in 'postal_city', with: 'Sixth'
    page.select('ACT', from: 'postal_state')
    fill_in 'postal_postcode', with: 'Seventh'
    #fill_in 'phone', with: 'Eighth'
    fill_in 'fax', with: 'Ninth'
    #fill_in 'email', with: 'Tenth'
    fill_in 'website', with: 'Eleventh'
    fill_in 'president_name', with: 'Twelfth'
    fill_in 'president_contact', with: 'Thirteenth'
    fill_in 'chairman_name', with: 'Fourteenth'
    fill_in 'chairman_contact', with: 'Fifteenth'
    fill_in 'secretary_name', with: 'Sixteenth'
    fill_in 'secretary_contact', with: 'Seventeenth'
    fill_in 'treasurer_name', with: 'Eighteenth'
    fill_in 'treasurer_contact', with: 'Nineteenth'
    click_button 'Save'
    expect(page).to have_content("error")
  end

end
