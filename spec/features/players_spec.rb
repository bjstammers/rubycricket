require 'rails_helper'

RSpec.feature "Players", type: :feature do

  scenario 'players index' do
    visit players_path
    expect(page).to have_content('First Name')
    expect(page).to have_content('Last Name')
    expect(page).to have_content('Display Name')
    expect(page).to have_content('Phone')
    expect(page).to have_content('Email')
    expect(page).to have_link('New')
  end

  feature 'create player' do
    scenario 'check form structure' do
      visit new_player_path
      expect(page).to have_button('Save')
      expect(page).to have_field('first_name')
      expect(page).to have_field('last_name')
      expect(page).to have_field('full_name')
      expect(page).to have_field('display_name')
      expect(page).to have_field('phone')
      expect(page).to have_field('email')
      expect(page).to have_field('birth_date')
      expect(page).to have_field('club_member')
      expect(page).to have_field('date_joined')
      expect(page).to have_select('club_id')
    end
    scenario 'populate and submit form' do
      visit new_player_path
      page.select('Test Club', from: 'club_id')
      fill_in 'first_name', with: 'Joe'
      fill_in 'last_name', with: 'Bloggs'
      fill_in 'full_name', with: 'Joe Bloggs'
      fill_in 'display_name', with: 'J. Bloggs'
      fill_in 'phone', with: '111111111'
      fill_in 'email', with: 'a@b.com'
      fill_in 'birth_date', with: '01/01/2000'
      fill_in 'club_member', with: true
      fill_in 'date_joined', with: '01/01/2018'
      click_button 'Save'
      expect(page).to have_content("success")

    end
  end
   scenario 'edit player' do
      @player = FactoryBot.create(:player)
      visit edit_player_path(1)
      fill_in 'first_name', with: 'Fred'
      fill_in 'last_name', with: 'Smith'
      fill_in 'full_name', with: 'Fred Smith'
      fill_in 'display_name', with: 'F. Smith'
      fill_in 'phone', with: '2222222222'
      fill_in 'email', with: 'd@e.com'
      fill_in 'birth_date', with: '02/02/2000'
      fill_in 'club_member', with: true
      fill_in 'date_joined', with: '02/02/2018'
      click_button 'Save'
      expect(page).to have_content("success")
   end

  scenario 'invalid player' do
    visit new_player_path
    #fill_in 'first_name', with: 'Joe'
    fill_in 'last_name', with: 'Bloggs'
    fill_in 'full_name', with: 'Joe Bloggs'
    fill_in 'display_name', with: 'J. Bloggs'
    #fill_in 'phone', with: '111111111'
    fill_in 'email', with: 'a@b.com'
    fill_in 'birth_date', with: '01/01/2000'
    fill_in 'club_member', with: true
    fill_in 'date_joined', with: '01/01/2018'
    click_button 'Save'
    expect(page).to have_content("error")
  end

end
