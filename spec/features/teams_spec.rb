require 'rails_helper'

RSpec.feature "Teams", type: :feature do

  scenario 'teams index' do
    visit teams_path
    expect(page).to have_content('Name')
    expect(page).to have_content('Grade')
    expect(page).to have_content('Captain')
    expect(page).to have_content('Vice Captain')
    expect(page).to have_link('New')
  end

  feature 'create team' do
    scenario 'check form structure' do
      visit new_team_path
      expect(page).to have_button('Save')
      expect(page).to have_field('team_name')
      expect(page).to have_field('grade')
      expect(page).to have_field('captain')
      expect(page).to have_field('vice_captain')
      #expect(page).to have_select('captain')
      #expect(page).to have_select('vice_captain')
    end
    scenario 'populate and submit form' do
      visit new_team_path
      fill_in 'team_name', with: 'New Team'
      fill_in 'grade', with: '2nd Grade'
      fill_in 'captain', with: 'Jack Black'
      #page.select('ACT', from: 'captain')
      fill_in 'vice_captain', with: 'Frank Brown'
      #page.select('ACT', from: 'vice_captain')
      click_button 'Save'
      expect(page).to have_content("success")

    end
  end
   scenario 'edit team' do
      @team = FactoryBot.create(:team)
      visit edit_team_path(1)
      fill_in 'team_name', with: 'Another New Team'
      fill_in 'grade', with: '3rd grade'
      fill_in 'captain', with: 'Wayne Bruce'
      #page.select('ACT', from: 'captain')
      fill_in 'vice_captain', with: 'Kent Clark'
      #page.select('ACT', from: 'vice_captain')
      click_button 'Save'
      expect(page).to have_content("success")
   end

  scenario 'invalid team' do
    visit new_team_path
    #fill_in 'team_name', with: 'New Team'
    #fill_in 'grade', with: '2nd Grade'
    fill_in 'captain', with: 'Andy Wright'
    #page.select('ACT', from: 'captain')
    fill_in 'vice_captain', with: 'Brian Trumble'
    #page.select('ACT', from: 'vice_captain')
    click_button 'Save'
    expect(page).to have_content("error")
  end

end
