require 'rails_helper'

RSpec.feature "Competitions", type: :feature do

  scenario 'competitions index' do
    visit competitions_path
    expect(page).to have_content('Competition')
    expect(page).to have_content('Start date')
    expect(page).to have_content('End date')
    expect(page).to have_link('New')
  end

  feature 'create competition' do
    scenario 'check form structure' do
      visit new_competition_path
      expect(page).to have_button('Save')
      expect(page).to have_field('competition_name')
      expect(page).to have_field('competition_start_date')
      expect(page).to have_field('competition_end_date')
      expect(page).to have_field('competition_match_days')
      expect(page).to have_field('competition_limited_overs')
      expect(page).to have_field('competition_max_overs')
      expect(page).to have_field('competition_max_bowler_overs')
      expect(page).to have_field('competition_points_win')
      expect(page).to have_field('competition_points_draw')
      expect(page).to have_field('competition_points_lose')
      expect(page).to have_field('competition_restrict_bat')
      expect(page).to have_field('competition_restrict_bowl')
      expect(page).to have_field('competition_restrict_field')
      expect(page).to have_field('competition_allow_bonus')
      expect(page).to have_field('competition_bonus_criteria')
      expect(page).to have_field('competition_points_bonus')
    end
    scenario 'populate and submit form' do
      visit new_competition_path
      fill_in 'competition_name', with: 'Sample Competition'
      fill_in 'competition_start_date', with: '01/01/2018'
      fill_in 'competition_end_date', with: '01/04/2018'
      fill_in 'competition_match_days', with: '2 days'
      # fill_in 'competition_limited_overs', with: true
      fill_in 'competition_max_overs', with: 50
      fill_in 'competition_max_bowler_overs', with: 10
      fill_in 'competition_points_win', with: 4
      fill_in 'competition_points_draw', with: 2
      fill_in 'competition_points_lose', with: 0
      fill_in 'competition_restrict_bat', with: 'aaaaaaaa'
      fill_in 'competition_restrict_bowl', with: 'bbbbbb'
      fill_in 'competition_restrict_field', with: 'cccccccc'
      # fill_in 'competition_allow_bonus', with: true
      fill_in 'competition_bonus_criteria', with: 'dddddddddd'
      fill_in 'competition_points_bonus', with: 2
      click_button 'Save'
      expect(page).to have_content("success")
    end
  end
   scenario 'edit competition' do
      @competition = FactoryBot.create(:competition)
      visit edit_competition_path(1)
      fill_in 'competition_name', with: 'Sample Competition'
      fill_in 'competition_start_date', with: '01/01/2018'
      fill_in 'competition_end_date', with: '01/04/2018'
      fill_in 'competition_match_days', with: '2 days'
      # fill_in 'competition_limited_overs', with: true
      fill_in 'competition_max_overs', with: 50
      fill_in 'competition_max_bowler_overs', with: 10
      fill_in 'competition_points_win', with: 4
      fill_in 'competition_points_draw', with: 2
      fill_in 'competition_points_lose', with: 0
      fill_in 'competition_restrict_bat', with: 'aaaaaaaa'
      fill_in 'competition_restrict_bowl', with: 'bbbbbb'
      fill_in 'competition_restrict_field', with: 'cccccccc'
      # fill_in 'competition_allow_bonus', with: true
      fill_in 'competition_bonus_criteria', with: 'dddddddddd'
      fill_in 'competition_points_bonus', with: 2
      click_button 'Save'
      expect(page).to have_content("success")
   end

  scenario 'invalid competition' do
    visit new_competition_path
    # fill_in 'competition_name', with: 'Sample Competition'
    # fill_in 'competition_start_date', with: '01/01/2018'
    # fill_in 'competition_end_date', with: '01/04/2018'
    fill_in 'competition_match_days', with: '2 days'
    # fill_in 'competition_limited_overs', with: true
    fill_in 'competition_max_overs', with: 50
    fill_in 'competition_max_bowler_overs', with: 10
    fill_in 'competition_points_win', with: 4
    fill_in 'competition_points_draw', with: 2
    fill_in 'competition_points_lose', with: 0
    fill_in 'competition_restrict_bat', with: 'aaaaaaaa'
    fill_in 'competition_restrict_bowl', with: 'bbbbbb'
    fill_in 'competition_restrict_field', with: 'cccccccc'
    # fill_in 'competition_allow_bonus', with: true
    fill_in 'competition_bonus_criteria', with: 'dddddddddd'
    fill_in 'competition_points_bonus', with: 2
    click_button 'Save'
    expect(page).to have_content("error")
  end

end
