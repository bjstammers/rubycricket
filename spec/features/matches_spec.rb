require 'rails_helper'

RSpec.feature "Matches", type: :feature do

  scenario 'matches index' do
    visit fixture_matches_path(1)
    expect(page).to have_content('Round No')
    expect(page).to have_content('Start Date')
    expect(page).to have_content('End Date')
    expect(page).to have_content('Home Team')
    expect(page).to have_content('Away Team')
    expect(page).to have_content('Ground')
    expect(page).to have_link('New')
  end

  feature 'create match' do
    scenario 'check form structure' do
      visit new_fixture_match_path(1)
      expect(page).to have_button('Save')
      expect(page).to have_content('Round No')
      expect(page).to have_content('Start Date')
      expect(page).to have_content('End Date')
      expect(page).to have_content('Home Team')
      expect(page).to have_content('Away Team')
      expect(page).to have_content('Ground')
      expect(page).to have_field('match_referee1')
      expect(page).to have_field('match_referee2')
      expect(page).to have_field('match_scorer')
      expect(page).to have_field('match_result')
    end
    scenario 'populate and submit form' do
      visit new_fixture_match_path(1)
      fill_in 'match_referee1', with: 'John Brown'
      fill_in 'match_referee2', with: 'Jack Black'
      fill_in 'match_scorer', with: 'James Grey'
      fill_in 'match_result', with: 'Draw'
      click_button 'Save'
      expect(page).to have_content("success")
    end
  end
  scenario 'edit match' do
      @match = FactoryBot.create(:match)
      visit edit_match_path(@match.id)
      fill_in 'match_referee1', with: 'Bryan Smith'
      fill_in 'match_referee2', with: 'Bruce Johnson'
      fill_in 'match_scorer', with: 'Bill Hobson'
      fill_in 'match_result', with: 'Lose'
      click_button 'Save'
      expect(page).to have_content("success")
  end

  # scenario 'invalid team' do
  #   visit new_team_path
  #   fill_in 'captain', with: 'Andy Wright'
  #   fill_in 'vice_captain', with: 'Brian Trumble'
  #   click_button 'Save'
  #   expect(page).to have_content("error")
  # end

end
