require 'rails_helper'

RSpec.describe "keywords/show", type: :view do
  before(:each) do
    @keyword = assign(:keyword, Keyword.create!(
      :key_type => "Key Type",
      :key_subtype => "Key Subtype",
      :key_value => "Key Value"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Key Type/)
    expect(rendered).to match(/Key Subtype/)
    expect(rendered).to match(/Key Value/)
  end
end
