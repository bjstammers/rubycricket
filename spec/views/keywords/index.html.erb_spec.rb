require 'rails_helper'

RSpec.describe "keywords/index", type: :view do
  before(:each) do
    assign(:keywords, [
      Keyword.create!(
        :key_type => "Key Type",
        :key_subtype => "Key Subtype",
        :key_value => "Key Value"
      ),
      Keyword.create!(
        :key_type => "Key Type",
        :key_subtype => "Key Subtype",
        :key_value => "Key Value"
      )
    ])
  end

  it "renders a list of keywords" do
    render
    assert_select "tr>td", :text => "Key Type".to_s, :count => 2
    assert_select "tr>td", :text => "Key Subtype".to_s, :count => 2
    assert_select "tr>td", :text => "Key Value".to_s, :count => 2
  end
end
