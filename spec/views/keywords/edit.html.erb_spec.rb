require 'rails_helper'

RSpec.describe "keywords/edit", type: :view do
  before(:each) do
    @keyword = assign(:keyword, Keyword.create!(
      :key_type => "MyString",
      :key_subtype => "MyString",
      :key_value => "MyString"
    ))
  end

  it "renders the edit keyword form" do
    render

    assert_select "form[action=?][method=?]", keyword_path(@keyword), "post" do

      assert_select "input[name=?]", "keyword[key_type]"

      assert_select "input[name=?]", "keyword[key_subtype]"

      assert_select "input[name=?]", "keyword[key_value]"
    end
  end
end
