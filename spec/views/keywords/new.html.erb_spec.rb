require 'rails_helper'

RSpec.describe "keywords/new", type: :view do
  before(:each) do
    assign(:keyword, Keyword.new(
      :key_type => "MyString",
      :key_subtype => "MyString",
      :key_value => "MyString"
    ))
  end

  it "renders new keyword form" do
    render

    assert_select "form[action=?][method=?]", keywords_path, "post" do

      assert_select "input[name=?]", "keyword[key_type]"

      assert_select "input[name=?]", "keyword[key_subtype]"

      assert_select "input[name=?]", "keyword[key_value]"
    end
  end
end
