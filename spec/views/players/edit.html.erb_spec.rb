require 'rails_helper'

RSpec.describe "players/edit", type: :view do
  before(:each) do
    @player = assign(:player, Player.create!(
      :first_name => "MyString",
      :last_name => "MyString",
      :full_name => "MyString",
      :display_name => "MyString",
      :phone => "MyString",
      :email => "MyString",
      :club_member => false,
      :club => nil,
      :team => nil
    ))
  end

  it "renders the edit player form" do
    render

    assert_select "form[action=?][method=?]", player_path(@player), "post" do

      assert_select "input[name=?]", "player[first_name]"

      assert_select "input[name=?]", "player[last_name]"

      assert_select "input[name=?]", "player[full_name]"

      assert_select "input[name=?]", "player[display_name]"

      assert_select "input[name=?]", "player[phone]"

      assert_select "input[name=?]", "player[email]"

      assert_select "input[name=?]", "player[club_member]"

      assert_select "input[name=?]", "player[club_id]"

      assert_select "input[name=?]", "player[team_id]"
    end
  end
end
