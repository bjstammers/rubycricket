require 'rails_helper'

RSpec.describe "players/index", type: :view do
  before(:each) do
    assign(:players, [
      Player.create!(
        :first_name => "First Name",
        :last_name => "Last Name",
        :full_name => "Full Name",
        :display_name => "Display Name",
        :phone => "Phone",
        :email => "Email",
        :club_member => false,
        :club => nil,
        :team => nil
      ),
      Player.create!(
        :first_name => "First Name",
        :last_name => "Last Name",
        :full_name => "Full Name",
        :display_name => "Display Name",
        :phone => "Phone",
        :email => "Email",
        :club_member => false,
        :club => nil,
        :team => nil
      )
    ])
  end

  it "renders a list of players" do
    render
    assert_select "tr>td", :text => "First Name".to_s, :count => 2
    assert_select "tr>td", :text => "Last Name".to_s, :count => 2
    assert_select "tr>td", :text => "Full Name".to_s, :count => 2
    assert_select "tr>td", :text => "Display Name".to_s, :count => 2
    assert_select "tr>td", :text => "Phone".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
