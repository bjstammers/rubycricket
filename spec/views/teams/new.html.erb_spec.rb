require 'rails_helper'

RSpec.describe "teams/new", type: :view do
  before(:each) do
    assign(:team, Team.new(
      :name => "MyString",
      :grade => "MyString",
      :captain => "MyString",
      :vice_captain => "MyString",
      :club => nil
    ))
  end

  it "renders new team form" do
    render

    assert_select "form[action=?][method=?]", teams_path, "post" do

      assert_select "input[name=?]", "team[name]"

      assert_select "input[name=?]", "team[grade]"

      assert_select "input[name=?]", "team[captain]"

      assert_select "input[name=?]", "team[vice_captain]"

      assert_select "input[name=?]", "team[club_id]"
    end
  end
end
