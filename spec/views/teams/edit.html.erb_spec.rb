require 'rails_helper'

RSpec.describe "teams/edit", type: :view do
  before(:each) do
    @team = assign(:team, Team.create!(
      :name => "MyString",
      :grade => "MyString",
      :captain => "MyString",
      :vice_captain => "MyString",
      :club => nil
    ))
  end

  it "renders the edit team form" do
    render

    assert_select "form[action=?][method=?]", team_path(@team), "post" do

      assert_select "input[name=?]", "team[name]"

      assert_select "input[name=?]", "team[grade]"

      assert_select "input[name=?]", "team[captain]"

      assert_select "input[name=?]", "team[vice_captain]"

      assert_select "input[name=?]", "team[club_id]"
    end
  end
end
