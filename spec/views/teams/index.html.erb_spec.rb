require 'rails_helper'

RSpec.describe "teams/index", type: :view do
  before(:each) do
    assign(:teams, [
      Team.create!(
        :name => "Name",
        :grade => "Grade",
        :captain => "Captain",
        :vice_captain => "Vice Captain",
        :club => nil
      ),
      Team.create!(
        :name => "Name",
        :grade => "Grade",
        :captain => "Captain",
        :vice_captain => "Vice Captain",
        :club => nil
      )
    ])
  end

  it "renders a list of teams" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Grade".to_s, :count => 2
    assert_select "tr>td", :text => "Captain".to_s, :count => 2
    assert_select "tr>td", :text => "Vice Captain".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
