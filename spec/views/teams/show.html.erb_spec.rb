require 'rails_helper'

RSpec.describe "teams/show", type: :view do
  before(:each) do
    @team = assign(:team, Team.create!(
      :name => "Name",
      :grade => "Grade",
      :captain => "Captain",
      :vice_captain => "Vice Captain",
      :club => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Grade/)
    expect(rendered).to match(/Captain/)
    expect(rendered).to match(/Vice Captain/)
    expect(rendered).to match(//)
  end
end
