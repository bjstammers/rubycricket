require 'rails_helper'

RSpec.describe "clubs/show", type: :view do
  before(:each) do
    @club = assign(:club, Club.create!(
      :name => "Name",
      :street_address => "Street Address",
      :street_city => "Street City",
      :street_state => "Street State",
      :street_postcode => "Street Postcode",
      :postal_address => "Postal Address",
      :postal_city => "Postal City",
      :postal_state => "Postal State",
      :postal_postcode => "Postal Postcode",
      :phone => "Phone",
      :fax => "Fax",
      :email => "Email",
      :website => "Website",
      :president_name => "President Name",
      :president_contact => "President Contact",
      :chairman_name => "Chairman Name",
      :chairman_contact => "Chairman Contact",
      :secretary_name => "Secretary Name",
      :secretary_contact => "Secretary Contact",
      :treasurer_name => "Treasurer Name",
      :treasurer_contact => "Treasurer Contact"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Street Address/)
    expect(rendered).to match(/Street City/)
    expect(rendered).to match(/Street State/)
    expect(rendered).to match(/Street Postcode/)
    expect(rendered).to match(/Postal Address/)
    expect(rendered).to match(/Postal City/)
    expect(rendered).to match(/Postal State/)
    expect(rendered).to match(/Postal Postcode/)
    expect(rendered).to match(/Phone/)
    expect(rendered).to match(/Fax/)
    expect(rendered).to match(/Email/)
    expect(rendered).to match(/Website/)
    expect(rendered).to match(/President Name/)
    expect(rendered).to match(/President Contact/)
    expect(rendered).to match(/Chairman Name/)
    expect(rendered).to match(/Chairman Contact/)
    expect(rendered).to match(/Secretary Name/)
    expect(rendered).to match(/Secretary Contact/)
    expect(rendered).to match(/Treasurer Name/)
    expect(rendered).to match(/Treasurer Contact/)
  end
end
