require 'rails_helper'

RSpec.describe "clubs/index", type: :view do
  before(:each) do
    assign(:clubs, [
      Club.create!(
        :name => "Name",
        :street_address => "Street Address",
        :street_city => "Street City",
        :street_state => "Street State",
        :street_postcode => "Street Postcode",
        :postal_address => "Postal Address",
        :postal_city => "Postal City",
        :postal_state => "Postal State",
        :postal_postcode => "Postal Postcode",
        :phone => "Phone",
        :fax => "Fax",
        :email => "Email",
        :website => "Website",
        :president_name => "President Name",
        :president_contact => "President Contact",
        :chairman_name => "Chairman Name",
        :chairman_contact => "Chairman Contact",
        :secretary_name => "Secretary Name",
        :secretary_contact => "Secretary Contact",
        :treasurer_name => "Treasurer Name",
        :treasurer_contact => "Treasurer Contact"
      ),
      Club.create!(
        :name => "Name",
        :street_address => "Street Address",
        :street_city => "Street City",
        :street_state => "Street State",
        :street_postcode => "Street Postcode",
        :postal_address => "Postal Address",
        :postal_city => "Postal City",
        :postal_state => "Postal State",
        :postal_postcode => "Postal Postcode",
        :phone => "Phone",
        :fax => "Fax",
        :email => "Email",
        :website => "Website",
        :president_name => "President Name",
        :president_contact => "President Contact",
        :chairman_name => "Chairman Name",
        :chairman_contact => "Chairman Contact",
        :secretary_name => "Secretary Name",
        :secretary_contact => "Secretary Contact",
        :treasurer_name => "Treasurer Name",
        :treasurer_contact => "Treasurer Contact"
      )
    ])
  end

  it "renders a list of clubs" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Street Address".to_s, :count => 2
    assert_select "tr>td", :text => "Street City".to_s, :count => 2
    assert_select "tr>td", :text => "Street State".to_s, :count => 2
    assert_select "tr>td", :text => "Street Postcode".to_s, :count => 2
    assert_select "tr>td", :text => "Postal Address".to_s, :count => 2
    assert_select "tr>td", :text => "Postal City".to_s, :count => 2
    assert_select "tr>td", :text => "Postal State".to_s, :count => 2
    assert_select "tr>td", :text => "Postal Postcode".to_s, :count => 2
    assert_select "tr>td", :text => "Phone".to_s, :count => 2
    assert_select "tr>td", :text => "Fax".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Website".to_s, :count => 2
    assert_select "tr>td", :text => "President Name".to_s, :count => 2
    assert_select "tr>td", :text => "President Contact".to_s, :count => 2
    assert_select "tr>td", :text => "Chairman Name".to_s, :count => 2
    assert_select "tr>td", :text => "Chairman Contact".to_s, :count => 2
    assert_select "tr>td", :text => "Secretary Name".to_s, :count => 2
    assert_select "tr>td", :text => "Secretary Contact".to_s, :count => 2
    assert_select "tr>td", :text => "Treasurer Name".to_s, :count => 2
    assert_select "tr>td", :text => "Treasurer Contact".to_s, :count => 2
  end
end
