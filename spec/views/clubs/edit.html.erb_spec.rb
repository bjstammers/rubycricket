require 'rails_helper'

RSpec.describe "clubs/edit", type: :view do
  before(:each) do
    @club = assign(:club, Club.create!(
      :name => "MyString",
      :street_address => "MyString",
      :street_city => "MyString",
      :street_state => "MyString",
      :street_postcode => "MyString",
      :postal_address => "MyString",
      :postal_city => "MyString",
      :postal_state => "MyString",
      :postal_postcode => "MyString",
      :phone => "MyString",
      :fax => "MyString",
      :email => "MyString",
      :website => "MyString",
      :president_name => "MyString",
      :president_contact => "MyString",
      :chairman_name => "MyString",
      :chairman_contact => "MyString",
      :secretary_name => "MyString",
      :secretary_contact => "MyString",
      :treasurer_name => "MyString",
      :treasurer_contact => "MyString"
    ))
  end

  it "renders the edit club form" do
    render

    assert_select "form[action=?][method=?]", club_path(@club), "post" do

      assert_select "input[name=?]", "club[name]"

      assert_select "input[name=?]", "club[street_address]"

      assert_select "input[name=?]", "club[street_city]"

      assert_select "input[name=?]", "club[street_state]"

      assert_select "input[name=?]", "club[street_postcode]"

      assert_select "input[name=?]", "club[postal_address]"

      assert_select "input[name=?]", "club[postal_city]"

      assert_select "input[name=?]", "club[postal_state]"

      assert_select "input[name=?]", "club[postal_postcode]"

      assert_select "input[name=?]", "club[phone]"

      assert_select "input[name=?]", "club[fax]"

      assert_select "input[name=?]", "club[email]"

      assert_select "input[name=?]", "club[website]"

      assert_select "input[name=?]", "club[president_name]"

      assert_select "input[name=?]", "club[president_contact]"

      assert_select "input[name=?]", "club[chairman_name]"

      assert_select "input[name=?]", "club[chairman_contact]"

      assert_select "input[name=?]", "club[secretary_name]"

      assert_select "input[name=?]", "club[secretary_contact]"

      assert_select "input[name=?]", "club[treasurer_name]"

      assert_select "input[name=?]", "club[treasurer_contact]"
    end
  end
end
