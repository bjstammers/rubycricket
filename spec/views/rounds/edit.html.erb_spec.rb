require 'rails_helper'

RSpec.describe "rounds/edit", type: :view do
  before(:each) do
    @round = assign(:round, Round.create!(
      :round_no => "MyString",
      :competition => nil
    ))
  end

  it "renders the edit round form" do
    render

    assert_select "form[action=?][method=?]", round_path(@round), "post" do

      assert_select "input[name=?]", "round[round_no]"

      assert_select "input[name=?]", "round[competition_id]"
    end
  end
end
