require 'rails_helper'

RSpec.describe "rounds/index", type: :view do
  before(:each) do
    assign(:rounds, [
      Round.create!(
        :round_no => "Round No",
        :competition => nil
      ),
      Round.create!(
        :round_no => "Round No",
        :competition => nil
      )
    ])
  end

  it "renders a list of rounds" do
    render
    assert_select "tr>td", :text => "Round No".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
