require 'rails_helper'

RSpec.describe "rounds/show", type: :view do
  before(:each) do
    @round = assign(:round, Round.create!(
      :round_no => "Round No",
      :competition => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Round No/)
    expect(rendered).to match(//)
  end
end
