require 'rails_helper'

RSpec.describe "innings/edit", type: :view do
  before(:each) do
    @inning = assign(:inning, Inning.create!(
      :inning_no => 1,
      :batting_team => "MyString",
      :bowling_team => "MyString",
      :runs => 1,
      :wickets => 1,
      :byes => 1,
      :legbyes => 1,
      :no_balls => 1,
      :wides => 1,
      :score => "MyString",
      :match => nil
    ))
  end

  it "renders the edit inning form" do
    render

    assert_select "form[action=?][method=?]", inning_path(@inning), "post" do

      assert_select "input[name=?]", "inning[inning_no]"

      assert_select "input[name=?]", "inning[batting_team]"

      assert_select "input[name=?]", "inning[bowling_team]"

      assert_select "input[name=?]", "inning[runs]"

      assert_select "input[name=?]", "inning[wickets]"

      assert_select "input[name=?]", "inning[byes]"

      assert_select "input[name=?]", "inning[legbyes]"

      assert_select "input[name=?]", "inning[no_balls]"

      assert_select "input[name=?]", "inning[wides]"

      assert_select "input[name=?]", "inning[score]"

      assert_select "input[name=?]", "inning[match_id]"
    end
  end
end
