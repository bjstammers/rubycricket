require 'rails_helper'

RSpec.describe "innings/show", type: :view do
  before(:each) do
    @inning = assign(:inning, Inning.create!(
      :inning_no => 2,
      :batting_team => "Batting Team",
      :bowling_team => "Bowling Team",
      :runs => 3,
      :wickets => 4,
      :byes => 5,
      :legbyes => 6,
      :no_balls => 7,
      :wides => 8,
      :score => "Score",
      :match => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/Batting Team/)
    expect(rendered).to match(/Bowling Team/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/4/)
    expect(rendered).to match(/5/)
    expect(rendered).to match(/6/)
    expect(rendered).to match(/7/)
    expect(rendered).to match(/8/)
    expect(rendered).to match(/Score/)
    expect(rendered).to match(//)
  end
end
