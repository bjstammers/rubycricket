require 'rails_helper'

RSpec.describe "innings/index", type: :view do
  before(:each) do
    assign(:innings, [
      Inning.create!(
        :inning_no => 2,
        :batting_team => "Batting Team",
        :bowling_team => "Bowling Team",
        :runs => 3,
        :wickets => 4,
        :byes => 5,
        :legbyes => 6,
        :no_balls => 7,
        :wides => 8,
        :score => "Score",
        :match => nil
      ),
      Inning.create!(
        :inning_no => 2,
        :batting_team => "Batting Team",
        :bowling_team => "Bowling Team",
        :runs => 3,
        :wickets => 4,
        :byes => 5,
        :legbyes => 6,
        :no_balls => 7,
        :wides => 8,
        :score => "Score",
        :match => nil
      )
    ])
  end

  it "renders a list of innings" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Batting Team".to_s, :count => 2
    assert_select "tr>td", :text => "Bowling Team".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => 5.to_s, :count => 2
    assert_select "tr>td", :text => 6.to_s, :count => 2
    assert_select "tr>td", :text => 7.to_s, :count => 2
    assert_select "tr>td", :text => 8.to_s, :count => 2
    assert_select "tr>td", :text => "Score".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
