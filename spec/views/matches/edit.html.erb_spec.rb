require 'rails_helper'

RSpec.describe "matches/edit", type: :view do
  before(:each) do
    @match = assign(:match, Match.create!(
      :fixture => nil,
      :referee1 => "MyString",
      :referee2 => "MyString",
      :scorer => "MyString",
      :result => "MyString"
    ))
  end

  it "renders the edit match form" do
    render

    assert_select "form[action=?][method=?]", match_path(@match), "post" do

      assert_select "input[name=?]", "match[fixture_id]"

      assert_select "input[name=?]", "match[referee1]"

      assert_select "input[name=?]", "match[referee2]"

      assert_select "input[name=?]", "match[scorer]"

      assert_select "input[name=?]", "match[result]"
    end
  end
end
