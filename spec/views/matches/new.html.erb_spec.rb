require 'rails_helper'

RSpec.describe "matches/new", type: :view do
  before(:each) do
    assign(:match, Match.new(
      :fixture => nil,
      :referee1 => "MyString",
      :referee2 => "MyString",
      :scorer => "MyString",
      :result => "MyString"
    ))
  end

  it "renders new match form" do
    render

    assert_select "form[action=?][method=?]", matches_path, "post" do

      assert_select "input[name=?]", "match[fixture_id]"

      assert_select "input[name=?]", "match[referee1]"

      assert_select "input[name=?]", "match[referee2]"

      assert_select "input[name=?]", "match[scorer]"

      assert_select "input[name=?]", "match[result]"
    end
  end
end
