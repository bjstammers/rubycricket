require 'rails_helper'

RSpec.describe "matches/show", type: :view do
  before(:each) do
    @match = assign(:match, Match.create!(
      :fixture => nil,
      :referee1 => "Referee1",
      :referee2 => "Referee2",
      :scorer => "Scorer",
      :result => "Result"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/Referee1/)
    expect(rendered).to match(/Referee2/)
    expect(rendered).to match(/Scorer/)
    expect(rendered).to match(/Result/)
  end
end
