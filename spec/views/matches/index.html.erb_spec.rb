require 'rails_helper'

RSpec.describe "matches/index", type: :view do
  before(:each) do
    assign(:matches, [
      Match.create!(
        :fixture => nil,
        :referee1 => "Referee1",
        :referee2 => "Referee2",
        :scorer => "Scorer",
        :result => "Result"
      ),
      Match.create!(
        :fixture => nil,
        :referee1 => "Referee1",
        :referee2 => "Referee2",
        :scorer => "Scorer",
        :result => "Result"
      )
    ])
  end

  it "renders a list of matches" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Referee1".to_s, :count => 2
    assert_select "tr>td", :text => "Referee2".to_s, :count => 2
    assert_select "tr>td", :text => "Scorer".to_s, :count => 2
    assert_select "tr>td", :text => "Result".to_s, :count => 2
  end
end
