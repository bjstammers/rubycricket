require 'rails_helper'

RSpec.describe "competitions/index", type: :view do
  before(:each) do
    assign(:competitions, [
      Competition.create!(
        :name => "Name",
        :match_days => 2,
        :limited_overs => false,
        :restrict_bowl => "Restrict Bowl",
        :restrict_bat => "Restrict Bat",
        :restrict_field => "Restrict Field",
        :points_win => 3.5,
        :points_draw => 4.5,
        :points_lose => 5.5,
        :allow_bonus => false,
        :bonus_criteria => "Bonus Criteria",
        :points_bonus => 6.5
      ),
      Competition.create!(
        :name => "Name",
        :match_days => 2,
        :limited_overs => false,
        :restrict_bowl => "Restrict Bowl",
        :restrict_bat => "Restrict Bat",
        :restrict_field => "Restrict Field",
        :points_win => 3.5,
        :points_draw => 4.5,
        :points_lose => 5.5,
        :allow_bonus => false,
        :bonus_criteria => "Bonus Criteria",
        :points_bonus => 6.5
      )
    ])
  end

  it "renders a list of competitions" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "Restrict Bowl".to_s, :count => 2
    assert_select "tr>td", :text => "Restrict Bat".to_s, :count => 2
    assert_select "tr>td", :text => "Restrict Field".to_s, :count => 2
    assert_select "tr>td", :text => 3.5.to_s, :count => 2
    assert_select "tr>td", :text => 4.5.to_s, :count => 2
    assert_select "tr>td", :text => 5.5.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "Bonus Criteria".to_s, :count => 2
    assert_select "tr>td", :text => 6.5.to_s, :count => 2
  end
end
