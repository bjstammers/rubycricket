require 'rails_helper'

RSpec.describe "competitions/new", type: :view do
  before(:each) do
    assign(:competition, Competition.new(
      :name => "MyString",
      :match_days => 1,
      :limited_overs => false,
      :restrict_bowl => "MyString",
      :restrict_bat => "MyString",
      :restrict_field => "MyString",
      :points_win => 1.5,
      :points_draw => 1.5,
      :points_lose => 1.5,
      :allow_bonus => false,
      :bonus_criteria => "MyString",
      :points_bonus => 1.5
    ))
  end

  it "renders new competition form" do
    render

    assert_select "form[action=?][method=?]", competitions_path, "post" do

      assert_select "input[name=?]", "competition[name]"

      assert_select "input[name=?]", "competition[match_days]"

      assert_select "input[name=?]", "competition[limited_overs]"

      assert_select "input[name=?]", "competition[restrict_bowl]"

      assert_select "input[name=?]", "competition[restrict_bat]"

      assert_select "input[name=?]", "competition[restrict_field]"

      assert_select "input[name=?]", "competition[points_win]"

      assert_select "input[name=?]", "competition[points_draw]"

      assert_select "input[name=?]", "competition[points_lose]"

      assert_select "input[name=?]", "competition[allow_bonus]"

      assert_select "input[name=?]", "competition[bonus_criteria]"

      assert_select "input[name=?]", "competition[points_bonus]"
    end
  end
end
