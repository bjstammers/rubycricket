require 'rails_helper'

RSpec.describe "competitions/show", type: :view do
  before(:each) do
    @competition = assign(:competition, Competition.create!(
      :name => "Name",
      :match_days => 2,
      :limited_overs => false,
      :restrict_bowl => "Restrict Bowl",
      :restrict_bat => "Restrict Bat",
      :restrict_field => "Restrict Field",
      :points_win => 3.5,
      :points_draw => 4.5,
      :points_lose => 5.5,
      :allow_bonus => false,
      :bonus_criteria => "Bonus Criteria",
      :points_bonus => 6.5
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/Restrict Bowl/)
    expect(rendered).to match(/Restrict Bat/)
    expect(rendered).to match(/Restrict Field/)
    expect(rendered).to match(/3.5/)
    expect(rendered).to match(/4.5/)
    expect(rendered).to match(/5.5/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/Bonus Criteria/)
    expect(rendered).to match(/6.5/)
  end
end
